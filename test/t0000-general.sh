#!/bin/sh

test_description="Test universe manipulation"

. ./sharness.sh

BINDIR="../../target/debug"

test_expect_success "Generate a name" '
	"$BINDIR/librae-cli" namegen 100 -s suffix | grep -q "suffix$"
'

test_expect_success "Create universe" '
	"$BINDIR/librae-cli" universegen -t -e 3 sunrays > small-universe.mpack
'

test_expect_success "Create empire 1 view of universe" '
	"$BINDIR/librae-cli" find -u empire name Empire1 < small-universe.mpack > eid &&
	"$BINDIR/librae-cli" viewuniverse -e `cat eid` < small-universe.mpack > 1-view.mpack
'

test_expect_success "Create a perfect view of universe" '
	"$BINDIR/librae-cli" viewuniverse < small-universe.mpack > perfect-view.mpack
'

test_expect_success "Create an action capturing star system 2 for the glory of empire 1" '
	"$BINDIR/librae-cli" actiongen 2-battle.actions capture StarSystem2 Empire1 < perfect-view.mpack
'

test_expect_success "Create an action capturing star system 1 for the glory of empire 1" '
	"$BINDIR/librae-cli" actiongen 1-battle.actions capture StarSystem1 Empire1 < perfect-view.mpack
'

test_expect_success "Fail to capture star system 2 due to running orders from empire 2" '
	test_expect_code 1 "$BINDIR/librae-cli" act --validate Empire2 2-battle.actions < small-universe.mpack > fail-small1.mpack
'
# Disabled until production costs are stabilized
# 
# test_expect_success "Capture system 2" '
# 	"$BINDIR/librae-cli" act -V Empire1 2-battle.actions < small-universe.mpack > small1.mpack
# '
# 
# test_expect_success "Don't capture system 2 again" '
# 	test_expect_code 1 "$BINDIR/librae-cli" act -V Empire1 2-battle.actions < small1.mpack > bad.mpack
# '
# 
# test_expect_success "Create a new Empire 1 view of universe" '
# 	"$BINDIR/librae-cli" viewuniverse -e Empire1 < small1.mpack > new-empire1-view.mpack
# '
# 
# test_expect_success "Empire 1 should see star system 2 now" '
# 	"$BINDIR/librae-cli" actiongen repeated-2-battle.actions Empire1 capture StarSystem2 < new-empire1-view.mpack 
# '


test_done
