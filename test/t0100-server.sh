#!/bin/sh

test_description="Test universe manipulation"

. ./sharness.sh

BINDIR="../../target/debug"

test_expect_success "Generate and store the keys" '
	"$BINDIR/librae-cli" auth ensure c1 > c1 &&
	"$BINDIR/librae-cli" auth ensure c2 > c2 &&
	"$BINDIR/librae-cli" auth ensure c3 > c3
'

test_expect_success "Launch the server" '
	"$BINDIR/librae-cli" universegen -t -e 2 -s 30 sunrays > small-universe.mpack &&
	("$BINDIR/librae-cli" server -p $(cat c1) -p $(cat c2) load-universe < small-universe.mpack & echo $! > server.pid) &&
	sleep 5
'

test_expect_success "Get a view from the server" '
	"$BINDIR/librae-cli" client 127.0.0.1:4433 --authdata-name c1 get-view > view.cbor
'

test_expect_success "Submit empty action list" '
	"$BINDIR/librae-cli" actiongen actions.cbor nothing < view.cbor  &&
	"$BINDIR/librae-cli" client 127.0.0.1:4433 --authdata-name c1 set-actions > view.cbor < actions.cbor
'


test_expect_success "Fail to get a view from third client because it's not in the game" '
	"$BINDIR/librae-cli" client 127.0.0.1:4433  --authdata-name c2 get-view > view.cbor &&
	test_expect_code 1 "$BINDIR/librae-cli" client 127.0.0.1:4433 --authdata-name c3 get-view > view.cbor
'

kill `cat server.pid`

test_done
