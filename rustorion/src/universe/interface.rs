// frontend for manipulation of Universe data
// Intended to be type-safe, assuming you don't mix two different EntityStored objects

use crate::storage::*;
use crate::units::*;
use crate::universe;
use crate::universe::EconomicAsset;
use num_traits::{One, Zero};
use std::hash::{Hash, Hasher};

pub enum WinState<'a> {
	Winner(Empire<'a>),
	Draw,
	NoWinner,
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct StarSystem<'a> {
	pub data: &'a universe::StarSystem,
	pub interface: Universe<'a>,
}

impl<'a> std::fmt::Display for StarSystem<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}", self.data.name)
	}
}

impl<'a> Hash for StarSystem<'a> {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.data.id.hash(state);
	}
}

impl<'a> StarSystem<'a> {
	pub fn can_build_fleet(self, empire: Empire<'a>) -> bool {
		self.empire() == Some(empire)
	}

	/// Is system controlled enough to be captured
	pub fn can_capture(self, empire: Empire<'a>) -> bool {
		let fleets = self.fleets();
		!fleets.is_empty() && fleets.iter().all(|s| s.empire() == empire)
	}

	pub fn is_capital(self) -> bool {
		self.interface.universe.capitals_in_empires.parent(self.data.id).is_some()
	}

	pub fn empire(self) -> Option<Empire<'a>> {
		self.interface.universe.star_systems_in_empires.parent(self.data.id).map(|e_id| self.interface.empire(e_id))
	}

	pub fn fleets(self) -> Vec<Fleet<'a>> {
		self.interface
			.universe
			.fleets_in_star_systems
			.children(self.data.id)
			.iter()
			.map(|id| self.interface.fleet(*id))
			.collect()
	}

	pub fn fleets_of(self, empire: Empire) -> Vec<Fleet<'a>> {
		self.fleets().into_iter().filter(|fleet| fleet.empire() == empire).collect()
	}

	pub fn starlanes(self) -> Vec<StarSystem<'a>> {
		self.interface
			.universe
			.starlanes
			.links(self.data.id)
			.into_iter()
			.map(|ss_id| self.interface.star_system(ss_id))
			.collect()
	}

	pub fn starlane_to(self, other: StarSystem<'a>) -> bool {
		self.interface.universe.starlanes.linked(self.data.id, other.data.id)
	}

	pub fn population(self) -> Unit<Population> {
		self.planets().iter().map(|p| p.data.population).sum()
	}

	pub fn planets(self) -> Vec<Planet<'a>> {
		self.interface
			.universe
			.planets_in_star_systems
			.children(self.data.id)
			.into_iter()
			.map(|id| self.interface.planet(id))
			.collect()
	}

	pub fn capture_price(self) -> Unit<EnergyCredit> {
		let multiplier = if self.empire().is_some() { 2.0 } else { 1.0 };
		self.population().mul(multiplier).mul(5.0).into()
	}

	pub fn related_trade_events(self) -> Vec<TradeEvent<'a>> {
		self.interface
			.trade_events()
			.into_iter()
			.filter(|e| e.source_economy_agent().star_system() == self || e.target_economy_agent().star_system() == self)
			.collect()
	}

	pub fn related_migration_events(self) -> Vec<MigrationEvent<'a>> {
		self.interface
			.migration_events()
			.into_iter()
			.filter(|e| e.source_planet().star_system() == self || e.target_planet().star_system() == self)
			.collect()
	}

	/// cost to ship between two linked systems, ec/dm3
	pub fn breach_shipping_cost(self, linked_star_system: Self, empire: Option<Empire>) -> Option<Rational> {
		if self.starlane_to(linked_star_system) {
			let same_empire = linked_star_system.empire() == self.empire();
			let own_empire = linked_star_system.empire() == empire;
			let multiplier: Rational = if same_empire && own_empire {
				(1, 1)
			} else if same_empire {
				(3, 2)
			} else {
				(2, 1)
			}
			.into();
			let cost = self.interface.universe.breach_shipping_cost.mul(multiplier);
			Some(cost)
		} else {
			None
		}
	}

	/// path and cost to ship dm3 between two arbitrary systems
	pub fn shipping_path_cost(self, linked_star_system: Self, empire: Option<Empire<'a>>) -> Option<(Vec<StarSystem<'a>>, Rational)> {
		let path_cost = |path: &[ID<universe::StarSystem>]| {
			let none: Option<ID<universe::StarSystem>> = None;
			let (_, cost) = path.iter().fold((none, Rational::from(0)), |(prev_star_system_option, sum), next_star_system_id| {
				let next_sum = match prev_star_system_option {
					None => sum,
					Some(prev_star_system_id) => {
						let prev_star_system = self.interface.star_system(prev_star_system_id);
						let next_star_system = self.interface.star_system(*next_star_system_id);
						prev_star_system.breach_shipping_cost(next_star_system, empire).unwrap().add(Rational::from(sum))
					}
				};
				(Some(*next_star_system_id), next_sum)
			});
			cost
		};

		let parents = self.interface.universe.trading_parents_maps.get(&empire.map(|e| e.data.id)).unwrap().get(&self.data.id).unwrap();
		match pathfinding::directed::dijkstra::build_path(&linked_star_system.data.id, parents) {
			path if path.is_empty() => None,
			path => {
				let cost = path_cost(&path);
				Some((path.iter().map(|id| self.interface.star_system(*id)).collect(), cost))
			}
		}
	}
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct Empire<'a> {
	pub data: &'a universe::Empire,
	pub interface: Universe<'a>,
}

impl<'a> Hash for Empire<'a> {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.data.id.hash(state);
	}
}

impl<'a> Empire<'a> {
	pub fn star_systems(self) -> Vec<StarSystem<'a>> {
		self.interface
			.universe
			.star_systems_in_empires
			.children(self.data.id)
			.iter()
			.map(|id| self.interface.star_system(*id))
			.collect()
	}

	pub fn fleets(self) -> Vec<Fleet<'a>> {
		self.interface
			.universe
			.fleets_in_empires
			.children(self.data.id)
			.into_iter()
			.map(|s_id| self.interface.fleet(s_id))
			.collect()
	}

	pub fn capital(self) -> Option<StarSystem<'a>> {
		let id = self.interface.universe.capitals_in_empires.child(self.data.id);
		id.map(|id| self.interface.star_system(id))
	}
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct Fleet<'a> {
	pub data: &'a universe::Fleet,
	pub interface: Universe<'a>,
}

impl<'a> std::fmt::Display for Empire<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{} empire", self.data.name)
	}
}

impl<'a> std::fmt::Display for Fleet<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "fleet {}", self.data.name)
	}
}

impl<'a> std::fmt::Debug for Fleet<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.debug_struct("Fleet").field("id", &self.data.id).field("name", &self.data.name).finish()
	}
}

impl<'a> std::hash::Hash for Fleet<'a> {
	fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
		self.data.id.hash(state);
	}
}

impl<'a> Fleet<'a> {
	pub fn can_build_ships(self) -> bool {
		self.star_system().empire() == Some(self.empire())
	}

	pub fn empire(self) -> Empire<'a> {
		let id = self.interface.universe.fleets_in_empires.parent(self.data.id).expect("Internal inconsistency");
		self.interface.empire(id)
	}

	pub fn star_system(self) -> StarSystem<'a> {
		let id = self.interface.universe.fleets_in_star_systems.parent(self.data.id).expect("Internal inconsistency");
		self.interface.star_system(id)
	}

	pub fn ships(self) -> Vec<Ship<'a>> {
		self.interface.universe.ships_in_fleets.children(self.data.id).iter().map(|id| self.interface.ship(*id)).collect()
	}

	pub fn trading_data(self) -> universe::TradingData {
		// maintenance budget is hardcoded to 10% of the total fleet price
		let ship_multipliers: Rational = self.ships().iter().map(|s| s.data.model.price()).sum();
		let fleet_cost: Unit<EnergyCredit> = Unit::from_r_ceil(ship_multipliers.mul(self.interface.universe.ship_cost));
		let budget: Unit<EnergyCredit> = Unit::from_f_floor(fleet_cost.mul(0.1));
		let maintenance_cost: universe::GoodsStorage = self.ships().iter().map(|s| s.data.model.maintenance_cost()).sum();
		let good_budget = budget;
		let food_max_price = good_budget.div(maintenance_cost.food.cast());
		let food_demand = maintenance_cost.food;
		let fuel_max_price = good_budget.div(maintenance_cost.fuel.cast());
		let fuel_demand = maintenance_cost.fuel;

		universe::TradingData {
			food: universe::TradedGoodData {
				demands: vec![universe::Demand {
					budget: Some(self.empire().data.goods_storage.energy_credits),
					quantity: Some(food_demand.sub(self.data.goods_storage.food).max(0.into())),
					max_price: food_max_price,
					..Default::default()
				}],
				..Default::default()
			},
			fuel: universe::TradedGoodData {
				demands: vec![universe::Demand {
					budget: Some(self.empire().data.goods_storage.energy_credits),
					quantity: Some(fuel_demand.sub(self.data.goods_storage.fuel).max(0.into())),
					max_price: fuel_max_price,
					..Default::default()
				}],
				..Default::default()
			},
		}
	}
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct Ship<'a> {
	pub data: &'a universe::Ship,
	pub interface: Universe<'a>,
}

impl<'a> std::fmt::Debug for Ship<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.debug_struct("Ship").field("id", &self.data.id).field("name", &self.data.name).finish()
	}
}

impl<'a> std::hash::Hash for Ship<'a> {
	fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
		self.data.id.hash(state);
	}
}

impl<'a> Ship<'a> {
	pub fn fleet(self) -> Fleet<'a> {
		let id = self.interface.universe.ships_in_fleets.parent(self.data.id).expect("Internal inconsistency");
		self.interface.fleet(id)
	}
}

#[derive(Copy, Clone, Debug)]
pub struct Universe<'a> {
	pub universe: &'a universe::Universe,
}

impl<'a> PartialEq for Universe<'a> {
	fn eq(&self, other: &Self) -> bool {
		self.universe.turn_number == other.universe.turn_number // FIXME: in practice should work...
	}
}

impl<'a> Eq for Universe<'a> {}

impl<'a> Universe<'a> {
	pub fn fleet_cost(self) -> Unit<EnergyCredit> {
		self.universe.ship_cost.mul(2)
	}

	pub fn new(universe: &'a universe::Universe) -> Self {
		Self { universe }
	}

	pub fn planet(self, id: ID<universe::Planet>) -> Planet<'a> {
		let data = self.universe.get(id).expect("ID lookup failed");
		Planet { data, interface: self }
	}

	pub fn star_system(self, id: ID<universe::StarSystem>) -> StarSystem<'a> {
		let data = self.universe.get(id).expect("ID lookup failed");
		StarSystem { data, interface: self }
	}

	pub fn empire(self, id: ID<universe::Empire>) -> Empire<'a> {
		let data = self.universe.get(id).expect("ID lookup failed");
		Empire { data, interface: self }
	}

	pub fn ship(self, id: ID<universe::Ship>) -> Ship<'a> {
		let data = self.universe.get(id).expect("ID lookup failed");
		Ship { data, interface: self }
	}

	pub fn fleet(self, id: ID<universe::Fleet>) -> Fleet<'a> {
		let data = self.universe.get(id).expect("ID lookup failed");
		Fleet { data, interface: self }
	}

	pub fn fleets(self) -> Vec<Fleet<'a>> {
		self.universe.fleets.iter().map(|x| Fleet { interface: self, data: x.1 }).collect()
	}

	pub fn empires(self) -> Vec<Empire<'a>> {
		self.universe.empires.iter().map(|x| Empire { interface: self, data: x.1 }).collect()
	}

	pub fn star_systems(self) -> Vec<StarSystem<'a>> {
		self.universe.star_systems.iter().map(|x| StarSystem { interface: self, data: x.1 }).collect()
	}

	pub fn planets(self) -> Vec<Planet<'a>> {
		self.universe.planets.iter().map(|x| Planet { interface: self, data: x.1 }).collect()
	}

	pub fn win_state(self) -> WinState<'a> {
		match self.universe.win_state {
			crate::universe::WinState::Winner(empire_id) => WinState::Winner(self.empire(empire_id)),
			crate::universe::WinState::NoWinner => WinState::NoWinner,
			crate::universe::WinState::Draw => WinState::Draw,
		}
	}

	pub fn trade_events(self) -> Vec<TradeEvent<'a>> {
		self.universe
			.trade_events_by_buyer
			.values()
			.flat_map(|ts| ts.iter().map(|t| TradeEvent { data: t, interface: self }))
			.collect()
	}

	pub fn migration_events(self) -> Vec<MigrationEvent<'a>> {
		self.universe.migration_events.iter().map(|t| MigrationEvent { data: t, interface: self }).collect()
	}

	pub fn economy_agent(self, economy_agent: universe::EconomyAgent) -> EconomyAgent<'a> {
		match economy_agent {
			universe::EconomyAgent::Planet(id) => EconomyAgent::Planet(self.planet(id)),
			universe::EconomyAgent::Fleet(id) => EconomyAgent::Fleet(self.fleet(id)),
			universe::EconomyAgent::Government(id) => EconomyAgent::Government(self.empire(id)),
			universe::EconomyAgent::VoidChurchBranch(empire_id, star_system_id) => EconomyAgent::VoidChurchBranch(self.empire(empire_id), self.star_system(star_system_id)),
		}
	}
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct Planet<'a> {
	pub data: &'a universe::Planet,
	pub interface: Universe<'a>,
}

impl<'a> Hash for Planet<'a> {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.data.id.hash(state);
	}
}

impl<'a> Planet<'a> {
	pub fn star_system(self) -> StarSystem<'a> {
		let id = self.interface.universe.planets_in_star_systems.parent(self.data.id).expect("Internal inconsistency");
		self.interface.star_system(id)
	}

	/// Cosmic ray intensity at the surface level
	pub fn effective_cosmic_ray_intensity(self) -> Unit<CosmicRayIntensity> {
		let local_cosmic_ray_intensity: Rational = self.star_system().data.star_cosmic_ray_intensity.into();
		let core_spin_compensation: Rational = self.data.core_spin_speed.into();
		local_cosmic_ray_intensity.sub(core_spin_compensation).max(0.into()).into()
	}

	/// difference from ideal (earth) conditions in surface bio quality
	pub fn surface_bio_quality_ediff(self) -> Rational {
		Rational::ONE.sub(Rational::from(self.data.surface_bio_quality))
	}

	/// difference from ideal (earth) conditions in atmosphere bio quality
	pub fn atmosphere_bio_quality_ediff(self) -> Rational {
		Rational::ONE.sub(Rational::from(self.data.atmosphere_bio_quality))
	}

	/// Fuel cost to construct a basic dwelling for a nuclear family
	// 100 weeks for an earth-tier house, thus 100 Fuel
	pub fn construction_cost(self) -> Rational {
		// basic earthdwelling shaping and furniture
		let base_cost = Rational::from(100);

		let foundation = Rational::from(25).mul(self.surface_bio_quality_ediff());
		let atmo = Rational::from(25).mul(self.atmosphere_bio_quality_ediff());

		let cosmic_ray = Rational::from(25).mul(self.effective_cosmic_ray_intensity());
		base_cost.add(foundation).add(atmo).add(cosmic_ray)
	}

	// Cost of insurance for 1 human living on the planet
	// Energy is considered to be spent locally to cover living costs
	pub fn insurance_cost(self) -> Rational {
		let base_cost = Rational::from((1, 100));

		let local_cosmic_ray_intensity: Rational = self.star_system().data.star_cosmic_ray_intensity.into();
		let core_spin_compensation: Rational = self.data.core_spin_speed.into();
		let effective_cosmic_ray_intensity: Rational = local_cosmic_ray_intensity.sub(core_spin_compensation).max(0.into());
		let cosmic_ray_insurance = effective_cosmic_ray_intensity.div(1000);

		let surface_bio_quality_offset: Rational = Rational::from(1).sub(Rational::from(self.data.surface_bio_quality));
		let surface_bio_quality_insurance = surface_bio_quality_offset.div(10);

		let atmosphere_bio_quality_offset: Rational = Rational::from(1).sub(Rational::from(self.data.atmosphere_bio_quality));
		let atmosphere_bio_quality_insurance = Rational::from(atmosphere_bio_quality_offset).div(10);

		// tracing::warn!("{}", base_cost + cosmic_ray_insurance + surface_bio_quality_insurance + atmosphere_bio_quality_insurance);
		base_cost + cosmic_ray_insurance + surface_bio_quality_insurance + atmosphere_bio_quality_insurance
	}

	pub fn income_tax(self) -> Option<Rational> {
		self.star_system().empire().map(|e| Rational::from((e.data.tax_rate as i128, 100)))
	}

	// Effective cost to produce 1 Fuel
	pub fn fuel_cost_price(self) -> Rational {
		Rational::from(2).div(self.data.fuel_availability).mul(self.insurance_cost())
	}

	/// purchase fuel up to this amount
	pub fn fuel_to_purchase(self) -> Unit<Fuel> {
		// TODO: figure out a better estimate
		let multiplier = 1.0;
		self.data.population.mul(multiplier).into()
	}

	/// produce fuel up to this amount
	pub fn fuel_to_produce(self) -> Unit<Fuel> {
		let multiplier = 3.0;
		self.data.population.mul(multiplier).into()
	}

	/// don't sell stock below this
	pub fn fuel_to_stock(self) -> Unit<Fuel> {
		let multiplier = 2.0;
		self.data.population.mul(multiplier).into()
	}

	pub fn food_cost_price(self) -> Rational {
		Farm::maintenance_cost(self).add(Farm::workforce_cost(self).mul(self.insurance_cost()))
	}

	/// food consumed by 1 human
	pub fn food_consumption(self) -> Rational {
		1.into()
	}

	/// produce food up to this amount
	pub fn food_to_produce(self) -> Unit<Food> {
		let multiplier = 5.0;
		self.data.population.mul(multiplier).into()
	}

	/// purchase food up to this amount
	pub fn food_to_purchase(self) -> Unit<Food> {
		let multiplier = 6.0;
		self.data.population.mul(multiplier).into()
	}

	/// don't sell stock below this
	pub fn food_to_stock(self) -> Unit<Food> {
		let multiplier = 2.0;
		self.data.population.mul(multiplier).into()
	}

	pub fn trading_data(self) -> universe::TradingData {
		let mut trading_data: universe::TradingData = Default::default();

		let food_shortage = Rational::from(self.data.population).sub(Rational::from(self.data.goods_storage.food)).max(Zero::zero());
		let insurance_fuel_shortage = Rational::from(self.data.population)
			.mul(self.insurance_cost())
			.sub(Rational::from(self.data.goods_storage.fuel))
			.max(Zero::zero());

		// will pay whatever possible to satisfy at least 25% of these

		let available_ec = self.data.goods_storage.energy_credits;

		let food_budget = Rational::from(available_ec).div(2);
		let fuel_budget = food_budget;
		if !food_shortage.is_zero() {
			trading_data.food.demands.push(universe::Demand {
				quantity: Some(Unit::from_r_ceil(food_shortage)),
				max_price: food_budget.div(food_shortage.div(4)),
				..Default::default()
			});
		};

		if !insurance_fuel_shortage.is_zero() {
			trading_data.food.demands.push(universe::Demand {
				quantity: Some(Unit::from_r_ceil(insurance_fuel_shortage)),
				max_price: fuel_budget.div(insurance_fuel_shortage.div(4)),
				..Default::default()
			});
		};

		let supply = self.data.goods_storage.food.sub(self.food_to_stock()).max(0.into());
		let min_price = self.food_cost_price();
		let demand = self.food_to_purchase().sub(self.data.goods_storage.food).max(0.into());
		let max_price = self.food_cost_price();
		let budget = self.data.goods_storage.energy_credits;
		trading_data.food.supplies.push(universe::Supply {
			quantity: supply,
			min_price,
			..Default::default()
		});
		trading_data.food.demands.push(universe::Demand {
			quantity: Some(demand),
			max_price,
			budget: Some(budget),
			..Default::default()
		});
		let supply = self.data.goods_storage.fuel.sub(self.fuel_to_stock()).max(0.into());
		let min_price = self.fuel_cost_price();
		let demand = self.fuel_to_purchase().sub(self.data.goods_storage.fuel).max(0.into());
		let max_price = self.fuel_cost_price();
		let budget = self.data.goods_storage.energy_credits;
		trading_data.fuel.supplies.push(universe::Supply {
			quantity: supply,
			min_price,
			..Default::default()
		});
		// special offer for the VC and whoever can compete!
		trading_data.fuel.supplies.push(universe::Supply {
			quantity: supply,
			min_price: min_price.min(One::one()),
			free_shipping: true,
			..Default::default()
		});
		trading_data.fuel.demands.push(universe::Demand {
			quantity: Some(demand),
			max_price,
			budget: Some(budget),
			..Default::default()
		});

		trading_data
	}
}

impl<'a> std::fmt::Display for Planet<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		let star_system_name = &self.star_system().data.name;
		write!(f, "{star_system_name} {}", self.data.number)
	}
}

#[derive(PartialEq, Eq, Clone, Copy)]
pub struct TradeEvent<'a> {
	pub data: &'a universe::TradeEvent,
	pub interface: Universe<'a>,
}

impl<'a> std::fmt::Display for TradeEvent<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		let (price, fee_price) = match self.data.cargo {
			universe::Cargo::Food(food) => (self.data.cost.div(f64::from(food)), self.data.fee.div(f64::from(food))),
			universe::Cargo::Fuel(fuel) => (self.data.cost.div(f64::from(fuel)), self.data.fee.div(f64::from(fuel))),
		};
		write!(
			f,
			"{} sold {} to {} for {} (price: {:.3}) (shipping: {}, price: {:.3})",
			self.source_economy_agent(),
			self.data.cargo,
			self.target_economy_agent(),
			self.data.cost,
			price,
			self.data.fee,
			fee_price,
		)
	}
}

impl<'a> TradeEvent<'a> {
	fn source_economy_agent(self) -> EconomyAgent<'a> {
		self.interface.economy_agent(self.data.source_economy_agent)
	}

	fn target_economy_agent(self) -> EconomyAgent<'a> {
		self.interface.economy_agent(self.data.target_economy_agent)
	}
}

#[derive(PartialEq, Eq, Clone, Copy)]
pub struct Demand<'a, TradeableUnit: universe::Tradeable> {
	pub data: &'a universe::Demand<TradeableUnit>,
	pub interface: Universe<'a>,
}

#[derive(PartialEq, Eq, Clone, Copy)]
pub struct Supply<'a, TradeableUnit: universe::Tradeable> {
	pub data: &'a universe::Supply<TradeableUnit>,
	pub interface: Universe<'a>,
}

#[derive(PartialEq, Eq, Clone, Copy)]
pub struct MigrationEvent<'a> {
	pub data: &'a universe::MigrationEvent,
	pub interface: Universe<'a>,
}

impl<'a> std::fmt::Display for MigrationEvent<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{} migrated from {} to {} for {}", self.data.population, self.source_planet(), self.target_planet(), self.data.fee,)
	}
}

impl<'a> MigrationEvent<'a> {
	fn source_planet(self) -> Planet<'a> {
		self.interface.planet(self.data.source_planet_id)
	}

	fn target_planet(self) -> Planet<'a> {
		self.interface.planet(self.data.target_planet_id)
	}
}

#[derive(Clone, Hash, PartialEq, Eq, Copy)]
pub enum EconomyAgent<'a> {
	Planet(Planet<'a>),
	Fleet(Fleet<'a>),
	Government(Empire<'a>),
	VoidChurchBranch(Empire<'a>, StarSystem<'a>),
}

impl<'a> std::fmt::Display for EconomyAgent<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			EconomyAgent::Planet(planet) => write!(f, "planet {}", planet),
			EconomyAgent::Fleet(fleet) => write!(f, "fleet {}", fleet),
			EconomyAgent::Government(empire) => write!(f, "{} government", empire),
			EconomyAgent::VoidChurchBranch(empire, _) => write!(f, "{} void church branch", empire),
		}
	}
}

impl<'a> EconomyAgent<'a> {
	pub fn trading_data(self) -> universe::TradingData {
		match self {
			EconomyAgent::Planet(planet) => planet.trading_data(),
			EconomyAgent::Fleet(fleet) => fleet.trading_data(),
			EconomyAgent::Government(_) => Default::default(),
			EconomyAgent::VoidChurchBranch(empire, _) => empire.interface.universe.void_church.trading_data(),
		}
	}

	pub fn star_system(self) -> StarSystem<'a> {
		match self {
			EconomyAgent::Planet(planet) => planet.star_system(),
			EconomyAgent::Fleet(fleet) => fleet.star_system(),
			EconomyAgent::Government(empire) => empire.capital().unwrap(),
			EconomyAgent::VoidChurchBranch(_, star_system) => star_system,
		}
	}

	pub fn empire(self) -> Option<Empire<'a>> {
		match self {
			EconomyAgent::Planet(planet) => planet.star_system().empire(),
			EconomyAgent::Fleet(fleet) => Some(fleet.empire()),
			EconomyAgent::Government(empire) => Some(empire),
			EconomyAgent::VoidChurchBranch(empire, _) => Some(empire),
		}
	}

	/// cost of transporting dm3 from partner to breach point
	pub fn exit_cost(self) -> Rational {
		match self {
			EconomyAgent::Planet(planet) => planet.interface.universe.breach_shipping_cost.mul(Rational::from((3, 10))),
			EconomyAgent::Fleet(fleet) => fleet.interface.universe.breach_shipping_cost.mul(Rational::from((1, 10))),
			EconomyAgent::Government(empire) => empire.interface.universe.breach_shipping_cost.mul(Rational::from((3, 10))),
			EconomyAgent::VoidChurchBranch(_, star_system) => star_system.interface.universe.breach_shipping_cost.mul(Rational::from((1, 10))),
		}
	}

	/// cost of transporting dm3 from breach point to partner, breach multiplier
	pub fn entry_cost(self) -> Rational {
		match self {
			EconomyAgent::Planet(planet) => planet.interface.universe.breach_shipping_cost.mul(Rational::from((2, 10))),
			EconomyAgent::Fleet(fleet) => fleet.interface.universe.breach_shipping_cost.mul(Rational::from((1, 10))),
			EconomyAgent::Government(empire) => empire.interface.universe.breach_shipping_cost.mul(Rational::from((3, 10))),
			EconomyAgent::VoidChurchBranch(_, star_system) => star_system.interface.universe.breach_shipping_cost.mul(Rational::from((1, 10))),
		}
	}

	/// Shipping cost of dm3 to self from target, breach multiplier
	pub fn shipping_cost(self, target: Self) -> Option<Rational> {
		if let Some((_path, cost)) = target.star_system().shipping_path_cost(self.star_system(), target.empire()) {
			Some(cost + self.exit_cost())
		} else {
			None
		}
	}
}
