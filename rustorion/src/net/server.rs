use crate::storage::*;
use anyhow::{anyhow, bail, Context, Result};
use futures::stream::futures_unordered::FuturesUnordered;
use futures_util::StreamExt;
use serde::{Deserialize, Serialize};
use state_machine::Handle;
use std::collections::HashSet;
use std::net::ToSocketAddrs;
use std::{sync::Arc, sync::Mutex};
use tokio::net::TcpListener;

#[derive(Debug, Clone)]
struct CustomVerifier;

impl rustls::server::danger::ClientCertVerifier for CustomVerifier {
	fn root_hint_subjects(&self) -> &[rustls::DistinguishedName] {
		&[]
	}

	fn verify_client_cert(
		&self,
		_end_entity: &rustls::pki_types::CertificateDer,
		_intermediates: &[rustls::pki_types::CertificateDer],
		_now: rustls::pki_types::UnixTime,
	) -> Result<rustls::server::danger::ClientCertVerified, rustls::Error> {
		Ok(rustls::server::danger::ClientCertVerified::assertion())
	}

	fn verify_tls12_signature(&self, _: &[u8], _: &rustls::pki_types::CertificateDer<'_>, _: &rustls::DigitallySignedStruct) -> Result<rustls::client::danger::HandshakeSignatureValid, rustls::Error> {
		Ok(rustls::client::danger::HandshakeSignatureValid::assertion())
	}

	fn verify_tls13_signature(&self, _: &[u8], _: &rustls::pki_types::CertificateDer<'_>, _: &rustls::DigitallySignedStruct) -> Result<rustls::client::danger::HandshakeSignatureValid, rustls::Error> {
		Ok(rustls::client::danger::HandshakeSignatureValid::assertion())
	}

	fn supported_verify_schemes(&self) -> Vec<rustls::SignatureScheme> {
		vec![rustls::SignatureScheme::ED25519]
	}
}

#[derive(clap::Parser)]
pub enum Command {
	LoadUniverse,
	CreateLobby,
}

#[derive(clap::Parser)]
pub struct Options {
	/// host:port to bind to
	#[clap(long = "bind", short = 'b', default_value = "0.0.0.0:4433")]
	pub addr: String,

	/// host:port to bind to for back-connections from clients
	#[clap(long = "bind-back", short = 'B', default_value = "0.0.0.0:4434")]
	pub subscribe_addr: String,

	/// authentication data file name, in storage. must be sane
	#[clap(default_value = "default-server", long)]
	pub authdata_name: String,

	#[clap(long = "add-player", short = 'p')]
	pub players: Vec<String>,

	#[clap(subcommand)]
	pub command: Command,
}

pub type PlayerID = [u8; 32];

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum Player {
	Human { player_id: PlayerID },
	Ai,
}

impl Player {
	pub fn human_str(&self) -> String {
		match self {
			Player::Human { player_id } => {
				let hexid = &hex::encode(player_id)[0..7];
				format!("ID: {}", hexid)
			}
			Player::Ai => "AI".into(),
		}
	}
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ConnectedClient {
	pub player_id: PlayerID,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum ServerStateView {
	Lobby(LobbyView),
	Game,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct LobbyView {
	pub slots: Vec<Option<Player>>,
	pub players_connected: Vec<PlayerID>,
}

#[derive(Serialize, Deserialize)]
pub struct GameState {
	universe: crate::universe::Universe,
	// mapping between players and empires they directly control
	player_map: bimap::BiMap<PlayerID, ID<crate::universe::Empire>>,
	ai_map: HashSet<ID<crate::universe::Empire>>,
}

#[derive(Serialize, Deserialize, Default)]
struct LobbyState {
	slots: Vec<Option<Player>>,
}

struct ServerState {
	data: ServerData,
	server_handle: state_machine::TokioHandle<Server>,
}

#[derive(Serialize, Deserialize)]
struct ServerData {
	state_n: u64,
	players_connected: std::collections::HashMap<PlayerID, u64>,
	server_state_enum: ServerStateEnum,
}

#[derive(Serialize, Deserialize)]
#[allow(clippy::large_enum_variant)]
enum ServerStateEnum {
	Game(GameState),
	Lobby(LobbyState),
}

impl Default for ServerData {
	fn default() -> ServerData {
		ServerData {
			state_n: 0,
			server_state_enum: ServerStateEnum::Lobby(LobbyState::default()),
			players_connected: Default::default(),
		}
	}
}

impl ServerState {
	/// let clients know the state needs to be redownloaded. Use only when something that goes into view changes
	fn bump_version(&mut self) {
		self.data.state_n += 1;
		self.server_handle.broadcast_event(ServerOutEvent::StateChanged(self.data.state_n));
	}

	// save game state to a proper file in server data directory
	fn save(&self, file_name: &str) -> Result<()> {
		std::fs::create_dir_all(crate::project_dir()?.data_dir().join("games"))?;
		let full_file_name = crate::project_dir()?.data_dir().join("games").join(file_name);
		let mut file = std::fs::File::create(full_file_name)?;
		ciborium::into_writer(&self.data, &mut file)?;
		Ok(())
	}

	fn view(&self) -> ServerStateView {
		match &self.data.server_state_enum {
			ServerStateEnum::Game(_) => ServerStateView::Game,
			ServerStateEnum::Lobby(LobbyState { slots }) => ServerStateView::Lobby(LobbyView {
				slots: slots.clone(),
				players_connected: self.data.players_connected.keys().cloned().collect(),
			}),
		}
	}
}

impl GameState {
	pub fn from_universe(universe: crate::universe::Universe) -> Self {
		GameState {
			universe,
			player_map: Default::default(),
			ai_map: Default::default(),
		}
	}
	fn make_turn(&mut self) -> Result<()> {
		self.universe.make_turn()?;
		Ok(())
	}

	fn get_empire_id(&self, player_id: &PlayerID) -> Option<ID<crate::universe::Empire>> {
		self.player_map.get_by_left(player_id).cloned()
	}

	fn find_free_empire(&mut self, player: &Player) -> Option<ID<crate::universe::Empire>> {
		for empire in &self.universe.empires {
			let id: ID<crate::universe::Empire> = ID::<crate::universe::Empire>::from_id(*empire.0);
			if !self.player_map.contains_right(&id) && !self.ai_map.contains(&id) {
				match player {
					Player::Human { player_id } => {
						self.player_map.insert(*player_id, id);
						return Some(id);
					}
					Player::Ai => {
						self.ai_map.insert(id);
						return Some(id);
					}
				}
			}
		}
		None
	}
}

impl Client {}

// fn load_certs(certs_data: &[u8]) -> io::Result<Vec<Certificate>> {
// 	certs(&mut BufReader::new(certs_data))
// 		.map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "invalid cert"))
// }
//
// fn load_keys(keys_data: &[u8]) -> io::Result<Vec<PrivateKey>> {
// 	pkcs8_private_keys(&mut BufReader::new(keys_data))
// 		.map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "invalid key"))
// }

struct RustorionServerProtocol;

impl cborpc::Protocol for RustorionServerProtocol {
	type State = Client;
	fn name(&self) -> String {
		"rustorion-server-0".into()
	}
	fn method(&mut self, client: &mut Client, method_name: &str, message: &[u8]) -> Result<cborpc::CallResponse> {
		let mut state = client.state_rc.lock().unwrap();
		match method_name {
			"get_affero" => {
				return Ok(cborpc::CallResponse {
					success: true,
					message: ciborium::into_vec(crate::AFFERO)?,
				})
			}
			"get_state" => {
				return Ok(cborpc::CallResponse {
					success: true,
					message: ciborium::into_vec(&state.view())?,
				})
			}
			_ => (),
		};

		match &mut state.data.server_state_enum {
			ServerStateEnum::Game(game_state) => match method_name {
				"get_actions" => {
					let empire_id = game_state.get_empire_id(&client.player_id).ok_or_else(|| anyhow!("Player does not control an empire"))?;
					let actions = game_state.universe.getf(empire_id)?.submitted_actions.clone();

					Ok(cborpc::CallResponse {
						success: true,
						message: ciborium::into_vec(&actions)?,
					})
				}

				"cancel_actions" => {
					let empire_id = game_state.get_empire_id(&client.player_id).ok_or_else(|| anyhow!("Player does not control an empire"))?;
					let empire = game_state.universe.getf_mut(empire_id)?;
					empire.submitted_actions = None;
					Ok(cborpc::CallResponse { success: true, message: vec![] })
				}
				"set_actions" => {
					let empire_id = game_state.get_empire_id(&client.player_id).ok_or_else(|| anyhow!("Player does not control an empire"))?.cast();
					let actions: Vec<crate::action::Action> = ciborium::from_reader(message)?;
					let actor = crate::action::Actor { empire_id };
					fn is_everyone_ready(empires: Vec<crate::universe::interface::Empire>, game_state: &GameState) -> bool {
						empires.iter().all(|e| game_state.ai_map.contains(&e.data.id) || e.data.submitted_actions.is_some())
					}

					game_state.universe.set_actions(Some(actions), &actor)?;
					if is_everyone_ready(game_state.universe.interface().empires(), game_state) {
						game_state.make_turn()?;
						state.bump_version();
						state.save("default")?;
					};

					Ok(cborpc::CallResponse { success: true, message: vec![] })
				}
				"get_view" => {
					let empire_id = game_state.get_empire_id(&client.player_id).ok_or_else(|| anyhow!("Player does not control an empire"))?;
					let u_view = crate::universeview::Universe::empire_view(&game_state.universe, empire_id);
					Ok(cborpc::CallResponse {
						success: true,
						message: ciborium::into_vec(&u_view)?,
					})
				}

				"get_turn" => Ok(cborpc::CallResponse {
					success: true,
					message: ciborium::into_vec(&game_state.universe.turn_number)?,
				}),
				_ => self.method_missing(method_name),
			},
			ServerStateEnum::Lobby(lobby_state) => match method_name {
				"add_slot" => {
					lobby_state.slots.push(None);
					state.bump_version();
					Ok(cborpc::CallResponse { success: true, message: vec![] })
				}
				"start_game" => {
					// TODO: real setup here
					let gen_params = crate::universegen::UniverseParameters {
						empires_n: lobby_state.slots.len() as u64,
						generator: crate::universegen::UniverseGenerator::Sunrays,
						..Default::default()
					};

					let universe = crate::universegen::generate_universe(&gen_params)?;

					let mut game_state = GameState {
						universe,
						player_map: Default::default(),
						ai_map: Default::default(),
					};

					for slot in &lobby_state.slots {
						let player = slot.as_ref().ok_or_else(|| anyhow!("Slot not filled"))?;
						game_state.find_free_empire(player).ok_or_else(|| anyhow!("not enough empires found"))?;
					}
					state.data.server_state_enum = ServerStateEnum::Game(game_state);
					state.bump_version();
					Ok(cborpc::CallResponse { success: true, message: vec![] })
				}
				"remove_slot" => {
					let slot_n: u64 = ciborium::from_reader(message)?;
					if slot_n >= lobby_state.slots.len() as u64 {
						Ok(cborpc::CallResponse {
							success: false,
							message: ciborium::into_vec("No such slot")?,
						})
					} else {
						lobby_state.slots.remove(slot_n as usize);
						state.bump_version();
						Ok(cborpc::CallResponse { success: true, message: vec![] })
					}
				}
				"assign_player_to_slot" => {
					let (player_id_opt, slot_n): (Option<PlayerID>, u64) = ciborium::from_reader(message)?;
					let slot_n = slot_n as usize;
					if slot_n >= lobby_state.slots.len() {
						bail!("Bad slot number");
					}
					lobby_state.slots[slot_n] = player_id_opt.map(|player_id| Player::Human { player_id });
					state.bump_version();
					Ok(cborpc::CallResponse { success: true, message: vec![] })
				}
				"assign_ai_to_slot" => {
					let slot_n: u64 = ciborium::from_reader(message)?;
					let slot_n = slot_n as usize;
					if slot_n >= lobby_state.slots.len() {
						bail!("Bad slot number");
					}
					lobby_state.slots[slot_n] = Some(Player::Ai);
					state.bump_version();
					Ok(cborpc::CallResponse { success: true, message: vec![] })
				}
				_ => self.method_missing(method_name),
			},
		}
	}
}

fn server_responder(client: Client) -> cborpc::Responder<Client> {
	let mut responder = cborpc::Responder::new(client);
	responder.add_protocol(RustorionServerProtocol);
	responder
}

async fn handle_back_connection(
	acceptor: tokio_rustls::TlsAcceptor,
	stream: tokio::net::TcpStream,
	// 	state_rc: Arc<Mutex<ServerState>>,
	server_handle: state_machine::TokioHandle<Server>,
) -> Result<()> {
	let tls_stream = acceptor.accept(stream).await?;
	let certv = tls_stream.get_ref().1.peer_certificates().ok_or_else(|| anyhow!("No peer certificate"))?;
	let cert_vec = certv[0].to_vec();
	let cert_hash = blake3::hash(&cert_vec);
	tracing::debug!("Cert incoming: {:?}", cert_hash);
	let client_back = ClientBack::new(); // cert_hash.as_bytes().clone(), state_rc);
	server_handle.add_subscriber(&client_back.driver.handle);
	client_back.run(tls_stream).await?;
	Ok(())
}

async fn handle_connection(acceptor: tokio_rustls::TlsAcceptor, stream: tokio::net::TcpStream, state_rc: Arc<Mutex<ServerState>>, _server_handle: state_machine::TokioHandle<Server>) -> Result<()> {
	let tls_stream = acceptor.accept(stream).await.context("Failed to accept a stream")?;
	let certv = tls_stream.get_ref().1.peer_certificates().ok_or_else(|| anyhow!("No peer certificate"))?;
	let cert_vec = certv[0].to_vec();
	let cert_hash = blake3::hash(&cert_vec);
	tracing::debug!("Cert incoming: {:?}", cert_hash);
	{
		let mut state = state_rc.lock().unwrap();
		let counter = state.data.players_connected.entry(*cert_hash.as_bytes()).or_default();
		*counter = counter.checked_add(1).ok_or_else(|| anyhow!("player counter overflow"))?;
		state.bump_version();
	}
	let client = Client::new(*cert_hash.as_bytes(), state_rc.clone());
	let result = client.run(tls_stream).await;
	let mut state = state_rc.lock().unwrap();
	let counter = state.data.players_connected.entry(*cert_hash.as_bytes()).or_default();
	*counter = counter.checked_sub(1).ok_or_else(|| anyhow!("player counter overflow"))?;
	state.bump_version();
	result
}

// struct ClientInEvent;

// struct ClientOutEvent;

type TlsStream = tokio_rustls::server::TlsStream<tokio::net::TcpStream>;

struct Client {
	player_id: PlayerID,
	state_rc: Arc<Mutex<ServerState>>,
	// 	driver: state_machine::TokioDriver<Self>,
}

impl Client {
	fn new(player_id: PlayerID, state_rc: Arc<Mutex<ServerState>>) -> Client {
		Client {
			player_id,
			state_rc,
			// 			driver: Default::default(),
		}
	}

	async fn run(self, tls_stream: TlsStream) -> Result<()> {
		let mut responder = server_responder(self);
		let (mut recv, mut send) = tokio::io::split(tls_stream);
		while let Some(success) = responder.answer_call(&mut recv, &mut send).await? {
			tracing::debug!("Call answered success={}", success);
		}
		Ok(())
	}
}

impl state_machine::CanSubscribe<Server> for ClientBack {
	fn translate_event(event: ServerOutEvent) -> Vec<ClientBackInEvent> {
		match event {
			ServerOutEvent::StateChanged(state_id) => vec![ClientBackInEvent::StateChanged(state_id)],
		}
	}
}

impl state_machine::AsyncEventMachine for Client {
	type InEvent = ();
	type OutEvent = ();
	type Driver = state_machine::TokioDriver<Self>;
}

#[derive(Clone, Debug)]
enum ClientBackInEvent {
	StateChanged(u64),
}

#[derive(Clone, Debug)]
struct ClientBackOutEvent;

struct ClientBack {
	// 	player_id: PlayerID,
	// 	state_rc: Arc<Mutex<ServerState>>,
	driver: state_machine::TokioDriver<Self>,
}

impl ClientBack {
	fn new(// 	player_id: PlayerID, state_rc: Arc<Mutex<ServerState>>
	) -> ClientBack {
		ClientBack {
			// 			player_id,
			// 			state_rc,
			driver: Default::default(),
		}
	}

	async fn run(mut self, tls_stream: TlsStream) -> Result<()> {
		let (recv, send) = tokio::io::split(tls_stream);
		let mut client = cborpc::Client::from_async_stream(recv, send);
		loop {
			match self.driver.guts.receiver.recv().await {
				Some(next_event) => match next_event {
					ClientBackInEvent::StateChanged(state_id) => {
						let mcall = cborpc::MethodCall {
							protocol_name: "rustorion-client-0".into(),
							method_name: "state_changed".into(),
							message: ciborium::into_vec(&state_id)?,
						};
						client.call(&mcall).await?;
					}
				},
				None => bail!("Receiver finished"),
			};
		}
	}
}

impl state_machine::AsyncEventMachine for ClientBack {
	type InEvent = ClientBackInEvent;
	type OutEvent = ClientBackOutEvent;
	type Driver = state_machine::TokioDriver<Self>;
}

pub struct Server {
	pub driver: state_machine::TokioDriver<Server>,
	pub client_futures: FuturesUnordered<tokio::task::JoinHandle<Result<()>>>,
	state: ServerState,
}

impl Default for Server {
	fn default() -> Server {
		let driver: state_machine::TokioDriver<Server> = Default::default();
		let server_handle = driver.handle.clone();
		Server {
			driver,
			client_futures: Default::default(),
			state: ServerState {
				server_handle,
				data: ServerData {
					state_n: Default::default(),
					players_connected: Default::default(),
					server_state_enum: ServerStateEnum::Lobby(Default::default()),
				},
			},
		}
	}
}

fn player_strings_to_players(player_strings: &[String]) -> Result<Vec<Player>> {
	let mut players = Vec::with_capacity(player_strings.len());
	for player_string in player_strings {
		players.push(match player_string.as_str() {
			"AI" => Player::Ai,
			other => {
				let player_id: PlayerID = hex::decode(other)?.try_into().map_err(|_| anyhow!("Bad PlayerID"))?;
				Player::Human { player_id }
			}
		});
	}
	Ok(players)
}

impl Server {
	pub fn setup_state(&mut self, options: &Options) -> Result<()> {
		match options.command {
			Command::LoadUniverse => {
				let universe = ciborium::from_reader(std::io::stdin())?;
				let mut game_state = GameState::from_universe(universe);
				for player in player_strings_to_players(&options.players)? {
					game_state.find_free_empire(&player).ok_or_else(|| anyhow!("Not enough empires"))?;
				}
				self.state.data.server_state_enum = ServerStateEnum::Game(game_state)
			}
			Command::CreateLobby => {
				let mut lobby = LobbyState::default();
				for player in player_strings_to_players(&options.players)? {
					lobby.slots.push(Some(player));
				}
				self.state.data.server_state_enum = ServerStateEnum::Lobby(lobby);
			}
		}
		Ok(())
	}

	pub async fn run(mut self, options: Options) -> Result<tokio::task::JoinHandle<Result<()>>> {
		// TODO: think about what if we need the rest
		let addr = options.addr.to_socket_addrs()?.next().ok_or_else(|| anyhow!("Failed to parse listen host"))?;

		let subscribe_addr = options.subscribe_addr.to_socket_addrs()?.next().ok_or_else(|| anyhow!("Failed to parse listen host"))?;

		let auth_data = crate::net::AuthData::obtain_by_name(&options.authdata_name)?;
		let state_rc = Arc::new(Mutex::new(self.state));

		let config = rustls::ServerConfig::builder_with_provider(Arc::new(rustls::crypto::ring::default_provider()))
			.with_safe_default_protocol_versions()
			.unwrap()
			.with_client_cert_verifier(Arc::new(CustomVerifier {}))
			.with_single_cert(vec![auth_data.cert.into()], auth_data.key.try_into().unwrap())?;
		let acceptor = tokio_rustls::TlsAcceptor::from(Arc::new(config));

		let client_acceptor = TcpListener::bind(&addr).await?;

		let client_back_acceptor = TcpListener::bind(&subscribe_addr).await?;

		let mut connections: FuturesUnordered<tokio::task::JoinHandle<Result<()>>> = FuturesUnordered::new();
		connections.push(tokio::task::spawn(futures::future::pending()));

		Ok(tokio::task::spawn(async move {
			loop {
				tokio::select!(
					client_result = connections.next() => match client_result {
						Some(Ok(Err(error))) => tracing::info!("Client process error: {:?}", error),
						Some(Ok(Ok(()))) => tracing::info!("Client disconnected"),
						Some(Err(error)) => tracing::warn!("Tokio error {:?}", error),
						None => panic!("This should never happen"),
					},
					stream_data = client_acceptor.accept() => connections.push(tokio::task::spawn(handle_connection(acceptor.clone(), stream_data?.0, state_rc.clone(), self.driver.handle.clone()))),
					stream_data = client_back_acceptor.accept() => connections.push(tokio::task::spawn(handle_back_connection(acceptor.clone(), stream_data?.0, self.driver.handle.clone()))),

					next_event =  self.driver.guts.receiver.recv() => {
						#[allow(clippy::match_single_binding)]
						match next_event.unwrap() {
							_ => ()
						}
					},
				);
			}
		}))
	}
}

#[derive(Clone, Debug)]
pub struct ServerInEvent;

#[derive(Clone, Debug)]
pub enum ServerOutEvent {
	StateChanged(u64),
}

impl state_machine::AsyncEventMachine for Server {
	type InEvent = ServerInEvent;
	type OutEvent = ServerOutEvent;
	type Driver = state_machine::TokioDriver<Server>;
}
