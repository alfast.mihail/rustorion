pub mod error;
pub mod links;

// this module is meant to be used without prefix, as its names are often used.

/*	Data structures that holds complexely interlinked data.
	Data is referenced by ID<DataType> and stored in types that implement
	EntityStored<DataType>.
	This is not generally type-tight.
	Survival guide:

	1. Don't store IDs outside of the structure
	2. Drop all IDs before deleting items.
	3. Take special care not to mix IDs between different instances of EntityStored.

	and generally you shouldn't get an error
*/

use anyhow::{Context, Result};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fmt;
use std::hash::{Hash, Hasher};
use std::marker::PhantomData;

pub type StorageID = u64;

#[derive(Deserialize, Serialize)]
#[serde(transparent)]
pub struct ID<Entity>(pub StorageID, #[serde(skip)] PhantomData<Entity>);

impl<Entity> Default for ID<Entity> {
	fn default() -> Self {
		Self::invalid()
	}
}

impl<Entity> PartialEq for ID<Entity> {
	fn eq(&self, other: &Self) -> bool {
		self.0 == other.0
	}
}

impl<Entity> Eq for ID<Entity> {}

impl<Entity> Copy for ID<Entity> {}

impl<Entity> Clone for ID<Entity> {
	fn clone(&self) -> ID<Entity> {
		*self
	}
}

impl<Entity> Hash for ID<Entity> {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.0.hash(state);
	}
}

impl<Entity> PartialOrd for ID<Entity> {
	fn partial_cmp(&self, other: &Self) -> Option<core::cmp::Ordering> {
		Some(self.0.cmp(&other.0))
	}
}

impl<Entity> Ord for ID<Entity> {
	fn cmp(&self, other: &Self) -> core::cmp::Ordering {
		self.0.cmp(&other.0)
	}
}

impl<Entity> fmt::Debug for ID<Entity> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		f.debug_struct("ID").field("id", &self.0).finish()
	}
}

impl<Entity> ID<Entity> {
	pub fn invalid() -> ID<Entity> {
		ID::<Entity>(0, PhantomData)
	}
	pub fn from_id(sid: StorageID) -> ID<Entity> {
		ID::<Entity>(sid, PhantomData)
	}
	pub fn cast<Entity2>(self) -> ID<Entity2> {
		ID::<Entity2>(self.0, PhantomData)
	}
}

pub trait IDentified
where
	Self: std::marker::Sized,
{
	fn id(&self) -> ID<Self>;
	fn id_mut(&mut self) -> &mut ID<Self>;
}

pub trait IssuesIDs {
	fn next_id(&mut self) -> StorageID;
}

pub trait EntityStored<EntityType>: IssuesIDs
where
	EntityType: IDentified,
{
	fn storage(&self) -> &HashMap<StorageID, EntityType>;
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, EntityType>;

	/// Obtain a reference to an entity by its id
	fn get(&self, id: ID<EntityType>) -> Option<&EntityType> {
		EntityStored::<EntityType>::storage(self).get(&id.0)
	}

	/// Obtain a mutable reference to an entity by its id
	fn get_mut(&mut self, id: ID<EntityType>) -> Option<&mut EntityType> {
		EntityStored::<EntityType>::storage_mut(self).get_mut(&id.0)
	}

	/// get() helper with an useful error
	fn getf(&self, id: ID<EntityType>) -> Result<&EntityType> {
		self.get(id).ok_or_else(|| error::EntityMissing { id: id.0 }.into())
	}

	/// get_mut() helper with an useful error
	fn getf_mut(&mut self, id: ID<EntityType>) -> Result<&mut EntityType> {
		self.get_mut(id).ok_or_else(|| error::EntityMissing { id: id.0 }.into())
	}

	// Destructor for an entity
	// Clean up all stuff related to an entity from Self
	fn unindex(&mut self, _entity_id: ID<EntityType>) -> Result<()> {
		Ok(())
	}

	// Constructor for an entity
	// Register entity in all Self indexes
	fn index(&mut self, _entity_id: ID<EntityType>) -> Result<()> {
		Ok(())
	}

	fn set(&mut self, entity: EntityType) -> Result<()> {
		let id = entity.id();
		if self.storage().contains_key(&id.0) {
			self.unindex(id).context("Unindexing failed")?;
		}

		EntityStored::<EntityType>::storage_mut(self).insert(id.0, entity);
		self.index(id).context("Indexing failed")
	}

	fn exists(&self, id: ID<EntityType>) -> bool {
		self.get(id).is_some()
	}

	fn insert(&mut self, mut entity: EntityType) -> Result<ID<EntityType>> {
		*entity.id_mut() = ID::<EntityType>(self.next_id(), PhantomData);
		let id = entity.id();
		self.set(entity)?;
		Ok(id)
	}

	fn remove(&mut self, id: ID<EntityType>) -> Result<()> {
		self.storage_mut().remove(&id.0).unwrap();
		self.unindex(id)?;
		Ok(())
	}

	fn all_ids(&self) -> Vec<ID<EntityType>> {
		self.storage().keys().map(|u| ID::<EntityType>::from_id(*u)).collect()
	}
}
