use anyhow::Result;

/// Manage authentication data
#[derive(clap::Parser)]
pub struct Options {
	#[clap(subcommand)]
	pub command: Command,
}

#[derive(clap::Parser)]
pub enum Command {
	/// Display hash of an authdata, generate it if it doesn't exist
	Ensure { name: String },
}

pub fn main(options: Options) -> Result<()> {
	match options.command {
		Command::Ensure { name } => {
			let auth_data = rustorion::net::AuthData::obtain_by_name(&name)?;
			println!("{}", hex::encode(auth_data.player_id()));
		}
	}
	Ok(())
}
