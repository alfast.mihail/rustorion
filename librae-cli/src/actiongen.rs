use anyhow::{bail, Context, Result};
use rustorion::action;
use std::fs;
use std::io;

#[derive(clap::Parser)]
#[clap(about = "Generate a rustorion action for view in STDIN and write/append it to given file")]
pub struct Options {
	file: String,

	#[clap(subcommand)]
	command: Command,
}

#[derive(clap::Parser)]
pub enum Command {
	Capture { star_system_name: String, empire_name: String },
	Move { fleet_name: String, star_system_name: String },
	BuildShips { ships_number: i64, fleet_name: String },
	// does not actually insert any action
	Nothing,
}

pub fn main(options: Options) -> Result<()> {
	let u = ciborium::from_reader(io::stdin())?;
	let ui = rustorion::universeview::interface::Universe { universe: &u };

	let mut actions: Vec<action::Action> = match fs::read(&options.file) {
		Ok(actions_bytes) => ciborium::from_reader(actions_bytes.as_slice())?,
		Err(ref e) if e.kind() == io::ErrorKind::NotFound => vec![],
		Err(e) => bail!("failed to read actions: {}", e),
	};
	match options.command {
		Command::Capture { star_system_name, empire_name } => {
			let empires = ui.empires();
			let empire = empires.iter().find(|e| e.data.name == empire_name).context("Failed to locate empire")?;
			let star_system = *ui.star_systems().iter().find(|ss| ss.data.name == star_system_name).context("Failed to locate star system")?;
			let a = action::Action::CaptureStarSystem {
				star_system_id: star_system.data.id.cast(),
				empire_id: empire.data.id.cast(),
			};
			actions.push(a);
		}
		Command::Move { fleet_name, star_system_name } => {
			let fleet = *ui.fleets().iter().find(|fleet| fleet.data.name == fleet_name).context("Failed to locate fleet")?;
			let star_system = *ui.star_systems().iter().find(|ss| ss.data.name == star_system_name).context("Failed to locate star system")?;
			let a = action::Action::MoveFleet {
				fleet_id: fleet.data.id.cast(),
				target_star_system_id: star_system.data.id.cast(),
			};
			actions.push(a);
		}
		Command::BuildShips { ships_number, fleet_name } => {
			let fleet = *ui.fleets().iter().find(|fleet| fleet.data.name == fleet_name).context("Failed to locate fleet")?;
			let a = action::Action::BuildShips {
				ships_number,
				fleet_id: fleet.data.id.cast(),
			};
			actions.push(a);
		}
		Command::Nothing => {}
	}
	let mut outfile = std::fs::File::create(options.file)?;
	ciborium::into_writer(&actions, &mut outfile)?;
	Ok(())
}
