use anyhow::Result;
use rustorion::net::server;

pub type Options = server::Options;

pub fn main(options: Options) -> Result<()> {
	let rt = tokio::runtime::Runtime::new().unwrap();
	rt.block_on(tokio_main(options))
}

pub async fn tokio_main(options: Options) -> Result<()> {
	let mut server = server::Server::default();
	server.setup_state(&options)?;

	let server_task = server.run(options).await?;
	tracing::info!("Server started");
	server_task.await??;
	Ok(())
}
