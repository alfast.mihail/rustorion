use anyhow::{bail, Result};
use rustorion::net::client;
use state_machine::Handle;

#[derive(clap::Parser)]
pub struct Options {
	host: String,

	/// authentication data file name, in storage. must be sane
	#[clap(default_value = "default-client", long)]
	authdata_name: String,

	#[clap(subcommand)]
	command: Command,
}

#[derive(clap::Parser)]
pub enum Command {
	/// Get your current universe view
	GetView,
	SetActions,
	/// Wait until turn changes, then print its number and exit
	WaitForTurn,
	/// Obtain your empire ID
	GetEmpireId,
	/// Request information on how to obtain the source code for the server
	GetAffero,
	/// Get current server state
	GetState,
}

pub fn main(options: Options) -> Result<()> {
	let rt = tokio::runtime::Runtime::new().unwrap();
	rt.block_on(tokio_main(options))
}

pub async fn tokio_main(options: Options) -> Result<()> {
	let auth_data = rustorion::net::AuthData::obtain_by_name(&options.authdata_name)?;

	let client = client::Client::default();
	let client_handle = client.0.handle.clone();

	let mut our_machine = state_machine::SimpleTokioMachine::<client::Client>::default().0;
	our_machine.handle.add_subscriber(&client_handle);
	client_handle.add_subscriber(&our_machine.handle);

	let mut client_join = tokio::task::spawn(client.connect(options.host, auth_data));
	match &options.command {
		Command::GetAffero => {
			our_machine.handle.broadcast_event(client::ClientInEvent::GetAffero);
			tokio::select! {
				event = our_machine.guts.receiver.recv() => {
					if let Some(client::ClientEvent::GotAffero(st)) = event {
						println!("Affero: {}", st);
					} else {
						bail!("Wrong event: {:?}", event);
					};
					Ok(())
				},
				err = &mut client_join => err?,
			}
		}
		Command::GetView => {
			our_machine.handle.broadcast_event(client::ClientInEvent::GetView);
			tokio::select! {
				event = our_machine.guts.receiver.recv() => {
					if let Some(client::ClientEvent::GotView(view)) = event {
						ciborium::into_writer(&view, &mut std::io::stdout())?;
					} else {
						bail!("Wrong event: {:?}", event);
					};
					Ok(())
				},
				err = &mut client_join => err?,
			}
		}
		Command::GetState => {
			our_machine.handle.broadcast_event(client::ClientInEvent::GetServerState);
			tokio::select! {
				event = our_machine.guts.receiver.recv() => {
					if let Some(client::ClientEvent::GotServerState(state)) = event {
						ciborium::into_writer(&state, &mut std::io::stdout())?;
					} else {
						bail!("Wrong event: {:?}", event);
					};
					Ok(())
				},
				err = &mut client_join => err?,
			}
		}
		Command::SetActions => {
			let actions = ciborium::from_reader(std::io::stdin())?;
			our_machine.handle.broadcast_event(client::ClientInEvent::SetActions(actions));
			tokio::select! {
				event = our_machine.guts.receiver.recv() => {

					if let Some(client::ClientEvent::ActionsSet) = event {} else {
						bail!("Wrong event: {:?}", event);
					};
					Ok(())
				},
				err = &mut client_join => err?,
			}
		}
		Command::WaitForTurn => {
			tokio::select! {
				event = our_machine.guts.receiver.recv() => {
					if let Some(client::ClientEvent::StateChanged(n)) = event {
						println!("State changed: {}", n);
					} else {
						bail!("Wrong event: {:?}", event);
					};
					Ok(())
				},
				err = &mut client_join => err?,
			}
		}
		Command::GetEmpireId => {
			our_machine.handle.broadcast_event(client::ClientInEvent::GetEmpireID);
			tokio::select! {
				event = our_machine.guts.receiver.recv() => {
					if let Some(client::ClientEvent::GotEmpireID(id)) = event {
						println!("{:?}", id);
					} else {
						bail!("Wrong event: {:?}", event);
					};
					Ok(())
				},
				err = &mut client_join => err?,
			}
		}
	}?;

	drop(client_join);

	Ok(())
}
