use anyhow::Result;
use rustorion::universegen;
use std::io;

pub type Options = universegen::UniverseParameters;

pub fn main(options: Options) -> Result<()> {
	let problems = universegen::check_params(&options);
	if !problems.is_empty() {
		tracing::warn!("Some problems found:");
	};
	for problem in problems {
		tracing::warn!("\t{}", problem);
	}
	let u = universegen::generate_universe(&options)?;

	ciborium::into_writer(&u, io::stdout())?;
	Ok(())
}
