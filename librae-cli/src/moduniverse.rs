use anyhow::Result;
use std::io;

#[derive(clap::Parser)]
#[clap(about = "Modify universe")]
pub struct Options {
	#[clap(subcommand)]
	command: Command,
}
#[derive(clap::Parser)]
pub enum Command {
	/// advance the turn
	MakeTurn {
		/// skip N turns
		#[clap(default_value = "1")]
		repeat: u64,
	},
}

pub fn main(options: Options) -> Result<()> {
	let mut u: rustorion::universe::Universe = ciborium::from_reader(io::stdin()).expect("Failed to parse universe");

	match options.command {
		Command::MakeTurn { repeat } => {
			for _ in 0..repeat {
				u.make_turn()?
			}
		}
	}

	ciborium::into_writer(&u, io::stdout())?;
	Ok(())
}
