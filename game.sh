#!/bin/sh
set -e
cargo build
PLAYER_ID=`cargo run --bin librae-cli -- auth ensure default-server`
cargo run --bin librae-cli -- universegen --seed 0 the-grid > universe.mpack
cd librae-gui
cargo run --bin librae-gui -- create-server -p AI -p $PLAYER_ID load-universe < ../universe.mpack
