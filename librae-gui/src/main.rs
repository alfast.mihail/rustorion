use anyhow::Result;
use clap::Parser;
use futures::StreamExt;
use gtk::gdk;
use gtk::gdk::prelude::*;
use gtk::gio;
use gtk::glib;
use gtk::graphene;
use gtk::gsk;
use gtk::pango;
use gtk::prelude::*;
use librae_gui::scene::*;
use num_traits::cast::ToPrimitive;
use num_traits::Zero;
use rustorion::net::{client, server};
use rustorion::storage::ID;
use rustorion::units::*;
use rustorion::universe;
use rustorion::universeview;
use state_machine::{CanSubscribe, FutureDriver, FutureHandle, Handle};
use std::cell::RefCell;
use std::collections::hash_set::HashSet;
use std::rc::Rc;
use tokio::io::AsyncReadExt;

fn from_universe(is: universe::UniverseLocation, interface: rustorion::universeview::interface::Universe) -> SceneCoords {
	SceneCoords {
		x: is.x + interface.universe.width as i64,
		y: is.y + interface.universe.height as i64,
	}
}

fn destroy_children(widget: &impl IsA<gtk::Widget>) {
	while let Some(child) = widget.first_child() {
		child.unparent();
	}
}

struct FullDark {
	node: gsk::RenderNode,
}

impl FullDark {
	fn new() -> Self {
		let snapshot = gtk::Snapshot::new();
		snapshot.append_color(&gdk::RGBA::new(0., 0., 0., 1.), &graphene::Rect::new(0., 0., 6000000., 6000000.));
		FullDark { node: snapshot.to_node().unwrap() }
	}
}

impl Drawable for FullDark {
	fn draw(&self, _scene: &Scene) -> Option<(gsk::RenderNode, DrawingData)> {
		Some((self.node.clone(), Default::default()))
	}

	fn z_index(&self) -> i64 {
		-100
	}
}

struct MoveOrder {
	node: gsk::RenderNode,
}

impl MoveOrder {
	fn new(from: ID<universeview::StarSystem>, to: ID<universeview::StarSystem>, empire_id: Option<ID<universeview::Empire>>, view_rc: Rc<universeview::Universe>) -> Self {
		let interface = view_rc.interface();

		let from = interface.star_system(from);
		let to = interface.star_system(to);

		let snapshot = gtk::Snapshot::new();

		let empire = empire_id.map(|e| interface.empire(e));
		let color = empire.map(|e| e.data.color.into()).unwrap_or((0.5, 0.5, 0.5));

		let first: SceneCoords = from_universe(from.data.location, interface);
		let second: SceneCoords = from_universe(to.data.location, interface);

		for offset in [(-10, -10), (10, -10), (0, 10)].iter() {
			let point = first + (*offset).into();
			snapshot.append_node(librae_gui::scene::draw_line(point, second, &gdk::RGBA::new(color.0, color.1, color.2, 0.7), 10));
		}

		let node = snapshot.to_node().unwrap();

		MoveOrder { node }
	}
}

impl Drawable for MoveOrder {
	fn draw(&self, _scene: &Scene) -> Option<(gsk::RenderNode, DrawingData)> {
		Some((self.node.clone(), Default::default()))
	}

	fn z_index(&self) -> i64 {
		-30
	}
}

struct Crown {
	node: (gsk::RenderNode, DrawingData),
}

impl Crown {
	fn new(id: ID<universeview::StarSystem>, view_rc: Rc<universeview::Universe>, scene: &Scene) -> Self {
		let interface = view_rc.interface();
		let ss = interface.star_system(id);
		let color = ss.empire().map(|e| e.data.color).unwrap_or_else(|| (0.6, 0.6, 0.6).into());
		let snapshot = gtk::Snapshot::new();

		scene.render_image("crown", &snapshot, 100, color);
		let node = (
			snapshot.to_node().unwrap(),
			DrawingData {
				coords: from_universe(ss.data.location, interface),
				size: DrawableSize::PixelSize((25., 25.).into()),
				alignment: Alignment::center(),
				offset: (0., -25.).into(),
				..Default::default()
			},
		);
		Crown { node }
	}
}

impl Drawable for Crown {
	fn draw(&self, _scene: &Scene) -> Option<(gsk::RenderNode, DrawingData)> {
		Some(self.node.clone())
	}

	fn z_index(&self) -> i64 {
		50
	}
}

#[derive(Clone)]
enum ReticleKind {
	StarSystem(StarSystem),
	Fleet(Fleet),
}

#[derive(Clone)]
struct Reticle {
	reticle_kind: ReticleKind,
	node: gsk::RenderNode,
}

impl Reticle {
	fn star_system(star_system: StarSystem, scene: &Scene) -> Self {
		let snapshot = gtk::Snapshot::new();
		draw_reticle(scene, &snapshot, 100);
		let node = snapshot.to_node().unwrap();
		Reticle {
			reticle_kind: ReticleKind::StarSystem(star_system),
			node,
		}
	}

	fn fleet(fleet: Fleet, scene: &Scene) -> Self {
		let snapshot = gtk::Snapshot::new();
		draw_reticle(scene, &snapshot, 100);
		let node = snapshot.to_node().unwrap();
		Reticle {
			reticle_kind: ReticleKind::Fleet(fleet),
			node,
		}
	}
}

impl Drawable for Reticle {
	fn draw(&self, scene: &Scene) -> Option<(gsk::RenderNode, DrawingData)> {
		let drawing_data = match &self.reticle_kind {
			ReticleKind::Fleet(fleet) => fleet.drawing_data(scene),
			ReticleKind::StarSystem(star_system) => star_system.drawing_data(scene),
		};
		drawing_data.map(|drawing_data| (self.node.clone(), drawing_data))
	}

	fn z_index(&self) -> i64 {
		60
	}
}

#[derive(Clone)]
struct StarSystem {
	id: ID<universeview::StarSystem>,
	handle: FutureHandle<GuiMachine>,
	star_node: (gsk::RenderNode, DrawingData),
	blip_node: (gsk::RenderNode, DrawingData),
}

impl StarSystem {
	fn new(id: ID<universeview::StarSystem>, view_rc: Rc<universeview::Universe>, handle: FutureHandle<GuiMachine>, scene: &Scene) -> Self {
		let interface = view_rc.interface();
		let ss = interface.star_system(id);
		let color = match ss.data.star_type {
			universeview::StarType::White => (1., 1., 1.).into(),
			universeview::StarType::Blue => (0.3, 0.3, 1.).into(),
			universeview::StarType::Yellow => (0.5, 0.5, 0.3).into(),
			universeview::StarType::Red => (1., 0.3, 0.3).into(),
		};
		let empire_color_option = ss.empire().map(|e| e.data.color);
		let coords = from_universe(ss.data.location, interface);

		let snapshot = gtk::Snapshot::new();
		if let Some(empire_color) = empire_color_option {
			scene.render_image("star", &snapshot, 100, empire_color);
		};
		let snapshot2 = gtk::Snapshot::new();
		scene.render_image("star", &snapshot2, 60, color);
		snapshot.translate(&graphene::Point::new(20., 20.));
		snapshot.append_node(snapshot2.to_node().unwrap());
		let star_node = (
			snapshot.to_node().unwrap(),
			DrawingData {
				coords,
				size: DrawableSize::PixelSize((50., 50.).into()),
				alignment: Alignment::center(),
				..Default::default()
			},
		);

		let snapshot = gtk::Snapshot::new();
		scene.render_image("starblip", &snapshot, 100, color);
		let blip_node = (
			snapshot.to_node().unwrap(),
			DrawingData {
				coords,
				size: DrawableSize::PixelSize((10., 10.).into()),
				alignment: Alignment::center(),
				..Default::default()
			},
		);

		StarSystem { id, handle, star_node, blip_node }
	}
}

impl Drawable for StarSystem {
	fn draw(&self, scene: &Scene) -> Option<(gsk::RenderNode, DrawingData)> {
		if scene.zoom_level < 5 {
			Some(self.blip_node.clone())
		} else {
			Some(self.star_node.clone())
		}
	}

	fn click(&self, number: u64) {
		match number {
			1 => self.handle.send_event(GuiInEvent::StarSystemSelected { id: self.id }),
			3 => self.handle.send_event(GuiInEvent::StarSystemTargeted { star_system_id: self.id }),
			_ => (),
		};
	}

	fn z_index(&self) -> i64 {
		25
	}
}

struct StarSystemName {
	node: (gsk::RenderNode, DrawingData),
}

impl StarSystemName {
	fn new(id: ID<universeview::StarSystem>, view_rc: Rc<universeview::Universe>) -> Self {
		let snapshot = gtk::Snapshot::new();
		let interface = view_rc.interface();
		let ss = interface.star_system(id);
		let coords = from_universe(ss.data.location, interface);
		let fontmap = pangocairo::FontMap::new();
		let context = fontmap.create_context();
		let layout = pango::Layout::new(&context);
		let font_desc = pango::FontDescription::from_string("Sans 12");
		layout.set_font_description(Some(&font_desc));
		layout.set_text(&ss.data.name);

		snapshot.append_layout(&layout, &gdk::RGBA::new(1., 1., 1., 1.));
		let dd = DrawingData {
			coords,
			size: DrawableSize::OriginalPixel,
			alignment: Alignment::center(),
			offset: (0., 30.).into(),
			..Default::default()
		};
		StarSystemName {
			node: (snapshot.to_node().unwrap(), dd),
		}
	}
}

impl Drawable for StarSystemName {
	fn z_index(&self) -> i64 {
		-45
	}

	fn draw(&self, scene: &Scene) -> Option<(gsk::RenderNode, DrawingData)> {
		if scene.zoom_level > 5 {
			Some(self.node.clone())
		} else {
			None
		}
	}
}

struct Starlanes {
	node: Option<gsk::RenderNode>,
}

impl Starlanes {
	fn new(view_rc: Rc<universeview::Universe>) -> Self {
		let interface = view_rc.interface();
		let snapshot = gtk::Snapshot::new();
		for star_system in interface.star_systems() {
			for linked_system_id in view_rc.starlanes.links(star_system.data.id) {
				let ss1 = star_system;
				let ss2 = interface.star_system(linked_system_id);
				let (l1, l2) = (from_universe(ss1.data.location, interface), from_universe(ss2.data.location, interface));
				snapshot.append_node(librae_gui::scene::draw_line(l1, l2, &gdk::RGBA::new(0.5, 0.5, 0.5, 1.0), 5));
			}
		}
		let node = snapshot.to_node();
		Starlanes { node }
	}
}

impl Drawable for Starlanes {
	fn draw(&self, _scene: &Scene) -> Option<(gsk::RenderNode, DrawingData)> {
		self.node.as_ref().map(|node| (node.clone(), Default::default()))
	}

	fn z_index(&self) -> i64 {
		-50
	}
}

fn draw_reticle(scene: &Scene, snapshot: &gtk::Snapshot, size: i32) {
	scene.render_image("reticle", snapshot, size, (1., 0., 0.).into());
}

#[derive(Clone)]
struct Fleet {
	fleet_id: ID<universeview::Fleet>,
	stack_number: u64,
	handle: FutureHandle<GuiMachine>,
	node: (gsk::RenderNode, DrawingData),
}

impl Fleet {
	fn new(fleet_id: ID<universeview::Fleet>, view_rc: Rc<universeview::Universe>, stack_number: u64, handle: FutureHandle<GuiMachine>, scene: &Scene) -> Self {
		let interface = view_rc.interface();
		let fleet = interface.fleet(fleet_id);
		let ss = fleet.star_system();
		let color = fleet.empire().data.color;
		let snapshot = gtk::Snapshot::new();
		scene.render_image("ship", &snapshot, 100, color);
		let coords = from_universe(ss.data.location, interface);
		let drawing_data = DrawingData {
			size: DrawableSize::PixelSize((25., 25.).into()),
			coords,
			offset: (20., -20.).into(),
			stack_number,
			..Default::default()
		};
		let node = (snapshot.to_node().unwrap(), drawing_data);

		Fleet { fleet_id, stack_number, handle, node }
	}
}

impl Drawable for Fleet {
	fn draw(&self, _scene: &Scene) -> Option<(gsk::RenderNode, DrawingData)> {
		Some(self.node.clone())
	}

	fn click(&self, number: u64) {
		#[allow(clippy::single_match)]
		match number {
			1 => {
				self.handle.send_event(GuiInEvent::FleetSelected {
					fleet_id: self.fleet_id,
					stack_number: self.stack_number,
				});
			}
			_ => (),
		}
	}

	fn z_index(&self) -> i64 {
		51
	}
}

#[derive(clap::Parser)]
enum Action {
	LoadView,
	Connect {
		hostport: String,
		/// authentication data file name, in storage. must be sane
		#[clap(default_value = "default-client", long)]
		authdata_name: String,
	},
	CreateServer(rustorion::net::server::Options),
}

/// Run rustorion GUI client
#[derive(clap::Parser)]
struct Options {
	#[clap(subcommand)]
	action: Action,
}

fn colored_text(text: &str, fg_color: Option<rustorion::color::Color>, bg_color: Option<rustorion::color::Color>) -> String {
	let fg = fg_color.map(|c| format!(" foreground='{}'", c.to_html())).unwrap_or_default();
	let bg = bg_color.map(|c| format!(" background='{}'", c.to_html())).unwrap_or_default();
	format!("<span{}{}>{}</span>", fg, bg, text)
}

impl ViewState {
	fn update_actions(&mut self) {
		while let Some(child) = self.actions_list.first_child() {
			self.actions_list.remove(&child);
		}
		for drawable_index in self.action_drawables.clone().into_iter() {
			self.scene.remove_drawable(drawable_index);
		}
		self.action_drawables.clear();
		let mut arrows = HashSet::<(_, _, ID<universeview::StarSystem>)>::new();
		{
			let ui = self.view_rc.interface();
			for action in self.action_cache.actions() {
				self.actions_list.append(&gtk::Label::new(Some(&action.describe(&self.view_rc))));
				#[allow(clippy::single_match)]
				match action {
					rustorion::action::Action::MoveFleet { fleet_id, target_star_system_id } => {
						let fleet = ui.fleet(fleet_id.cast());
						arrows.insert((fleet.empire().data.id, fleet.star_system().data.id, target_star_system_id.cast()));
					}
					_ => (), // TODO: render smth
				}
			}
		};
		for (empire_id, from, to) in arrows {
			let move_order = MoveOrder::new(from, to, Some(empire_id), self.view_rc.clone());

			let index = self.scene.add_drawable(move_order);
			self.action_drawables.push(index);
		}
		self.scene.queue_draw();
	}

	fn add_action(&mut self, action: rustorion::action::Action) {
		self.action_cache
			.add(
				action,
				&self.view_rc,
				&rustorion::action::Actor {
					empire_id: self.view_rc.controlled_empire.unwrap(),
				},
			)
			.unwrap(); // TODO: no unwraps
		self.update_actions();
	}

	fn remove_action(&mut self, action: &rustorion::action::Action) {
		self.action_cache.remove(
			action,
			&self.view_rc,
			&rustorion::action::Actor {
				empire_id: self.view_rc.controlled_empire.unwrap(),
			},
		);
		self.update_actions();
	}
}

// TODO: rethink this?
impl GuiMachine {
	fn process_event(handle: &FutureHandle<Self>, application: &gtk::Application, state: &mut GuiMachineState, event: GuiInEvent) {
		let window = application.active_window().unwrap();

		match event {
			GuiInEvent::SetView { view, local } => {
				let info_widget = gtk::Box::new(gtk::Orientation::Horizontal, 0);
				let width = view.width * 2;
				let height = view.height * 2;

				let scene = librae_gui::scene::SceneWidget::new();
				scene.init(width, height);

				let actions_list = gtk::ListBox::new();

				let end_turn_checkbox = gtk::CheckButton::with_label("Ready");

				let scrolled_builder = gtk::ScrolledWindow::builder();

				let scrolled_builder = if let GuiMachineState::AwaitingView {
					scrolled: Some(previous_scrolled), ..
				} = state
				{
					scrolled_builder.hadjustment(&previous_scrolled.hadjustment()).vadjustment(&previous_scrolled.vadjustment())
				} else {
					scrolled_builder
				};

				let scrolled = scrolled_builder.build();

				let view_rc = Rc::new(view);
				let view_state = ViewState {
					scene: scene.clone(),
					scrolled: Some(scrolled.clone()),
					actions_list: actions_list.clone(),
					info_widget,
					view_rc: view_rc.clone(),
					action_cache: rustorion::action::ActionCache::new(&view_rc),
					action_drawables: vec![],
					end_turn_checkbox,
					focus: None,
				};

				let handle_clone = handle.clone();
				view_state.end_turn_checkbox.connect_toggled(move |checkbox| {
					if checkbox.is_active() {
						handle_clone.send_event(GuiInEvent::SetActions);
						checkbox.set_inconsistent(true);
					}
					// TODO: if unchecked, drop actions
				});

				let paned = gtk::Paned::new(gtk::Orientation::Vertical);
				scrolled.set_child(Some(&view_state.scene));
				scrolled.set_vexpand(true);

				if let GuiMachineState::AwaitingView { previous_zoom_level, .. } = state {
					if let Err(BadZoomLevel) = scene.update_zoom_level(*previous_zoom_level) {
						scene.update_zoom_level(0).unwrap_or(());
						tracing::warn!("previous_zoom_level is bad");
					}
				} else {
					scene.update_zoom_level(0).unwrap_or(());
				};

				let world_drag = gtk::GestureDrag::new();
				scrolled.add_controller(world_drag.clone());
				world_drag.set_touch_only(false);
				world_drag.set_propagation_phase(gtk::PropagationPhase::Capture);

				let scrolled_drag = scrolled.clone();
				let previous_d_x = Rc::new(RefCell::new(0.0));
				let previous_d_y = Rc::new(RefCell::new(0.0));
				let final_previous_d_x = previous_d_x.clone();
				let final_previous_d_y = previous_d_y.clone();

				world_drag.connect_drag_update(move |_g_d, delta_x, delta_y| {
					let adj = scrolled_drag.hadjustment();
					let mut pdx = previous_d_x.borrow_mut();

					adj.set_value(adj.value() - delta_x + *pdx);
					*pdx = delta_x;

					let adj = scrolled_drag.vadjustment();
					let mut pdy = previous_d_y.borrow_mut();

					adj.set_value(adj.value() - delta_y + *pdy);
					*pdy = delta_y;
				});

				world_drag.connect_drag_end(move |_g_d, _x, _y| {
					let mut pdx = final_previous_d_x.borrow_mut();
					*pdx = 0.0;
					let mut pdy = final_previous_d_y.borrow_mut();
					*pdy = 0.0;
				});

				let motion = gtk::EventControllerMotion::new();
				scrolled.add_controller(motion.clone());
				let last_seen_mouse = Rc::new(RefCell::new((0.0, 0.0)));
				let last_seen_mouse_motion = last_seen_mouse.clone();

				motion.connect_motion(move |_controller, x, y| {
					*last_seen_mouse_motion.borrow_mut() = (x, y);
				});

				let last_seen_mouse_motion = last_seen_mouse.clone();

				motion.connect_enter(move |_controller, x, y| {
					*last_seen_mouse_motion.borrow_mut() = (x, y);
				});

				let scroller = gtk::EventControllerScroll::new(gtk::EventControllerScrollFlags::VERTICAL | gtk::EventControllerScrollFlags::DISCRETE);
				scrolled.add_controller(scroller.clone());

				let scrolled_scroller = scrolled.clone();
				let scene_clone = view_state.scene.clone();
				scroller.connect_scroll(move |_controller, _dx, dy| {
					if dy != 0.0 {
						let full_position: ScaledCoords = (*last_seen_mouse.borrow()).into();

						if let Some(zoom) = (scene_clone.zoom_level() as i64).checked_add(0 - dy as i64).and_then(|i: i64| i.try_into().ok()) {
							let old_zoom = scene_clone.zoom();
							scene_clone.update_zoom_level(zoom).unwrap_or(());
							let adj = scrolled_scroller.hadjustment();
							let hoffset = adj.value();
							let adj = scrolled_scroller.vadjustment();
							let voffset = adj.value();

							let hoffset = (hoffset + full_position.x) * scene_clone.zoom() / old_zoom - full_position.x;
							let voffset = (voffset + full_position.y) * scene_clone.zoom() / old_zoom - full_position.y;
							let adj = scrolled_scroller.hadjustment();
							adj.set_upper(adj.upper() / old_zoom * scene_clone.zoom());
							let hoffset = hoffset.clamp(0.0, 0.0_f64.max(adj.upper() - adj.page_size()));
							adj.set_value(hoffset);
							let adj = scrolled_scroller.vadjustment();

							adj.set_upper(adj.upper() / old_zoom * scene_clone.zoom());
							let voffset = voffset.clamp(0.0, 0.0_f64.max(adj.upper() - adj.page_size()));
							adj.set_value(voffset);
						}
					}
					glib::Propagation::Stop
				});

				let arrow_controller = gtk::EventControllerKey::new();
				scrolled.add_controller(arrow_controller.clone());
				let scrolled_clone = scrolled.clone();

				arrow_controller.connect_key_pressed(move |_, key, _, _| {
					let hadj = scrolled_clone.hadjustment();
					let vadj = scrolled_clone.vadjustment();

					let var = match key {
						gdk::Key::Left => {
							hadj.set_value(hadj.value() - 10.0);
							true
						}
						gdk::Key::Right => {
							hadj.set_value(hadj.value() + 10.0);
							true
						}
						gdk::Key::Up => {
							vadj.set_value(vadj.value() - 10.0);
							true
						}
						gdk::Key::Down => {
							vadj.set_value(vadj.value() + 10.0);
							true
						}
						_ => false,
					};
					if var {
						glib::Propagation::Stop
					} else {
						glib::Propagation::Proceed
					}
				});

				scrolled.set_policy(gtk::PolicyType::Always, gtk::PolicyType::Always);

				paned.set_start_child(Some(&scrolled));
				paned.set_resize_start_child(true);
				paned.set_end_child(Some(&view_state.info_widget));
				paned.set_shrink_end_child(true);

				let vbox = gtk::Box::new(gtk::Orientation::Vertical, 1);
				vbox.set_homogeneous(false);
				let tophbox = gtk::Box::new(gtk::Orientation::Horizontal, 1);

				if !local {
					tophbox.prepend(&view_state.end_turn_checkbox);
				};

				tophbox.append(&gtk::Label::new(Some(&format!("Turn {}", view_rc.turn_number))));

				let infohbox = gtk::Box::new(gtk::Orientation::Horizontal, 1);

				let ui = view_state.view_rc.interface();
				let mut info_str = "".to_string();
				if let Some(empire) = ui.controlled_empire() {
					info_str.push_str(&colored_text(&empire.data.name, Some(empire.data.color), None));
					info_str.push_str(&format!(" EC: {}+{}", empire.data.goods_storage.energy_credits, empire.data.empire_economics.tax_income));
					info_str.push_str(&format!(" Fuel: {}", empire.data.goods_storage.fuel));
				};
				let info_label = gtk::Label::new(None);
				info_label.set_markup(&info_str);
				infohbox.prepend(&info_label);
				vbox.prepend(&tophbox);
				vbox.prepend(&infohbox);

				let winner_label = gtk::Label::new(None);
				match ui.win_state() {
					rustorion::universeview::interface::WinState::Winner(empire) => {
						winner_label.set_markup(&format!("<big>Empire {} won</big>", empire.data.name));
						vbox.prepend(&winner_label);
					}
					rustorion::universeview::interface::WinState::Draw => {
						winner_label.set_markup("<big>Nobody won</big>");
						vbox.prepend(&winner_label);
					}
					rustorion::universeview::interface::WinState::NoWinner => (),
				};

				let mut star_systems = vec![];
				let mut crowns = vec![];

				for star_system in ui.star_systems() {
					if star_system.is_capital() {
						crowns.push(star_system.data.id);
					}

					star_systems.push(star_system.data.id);

					for (stack_number, fleet) in star_system.fleets().into_iter().enumerate() {
						let fleet_drawable = Fleet::new(fleet.data.id, view_rc.clone(), stack_number as u64, handle.clone(), &scene.scene());
						scene.add_drawable(fleet_drawable);
					}
				}

				scene.add_drawable(Starlanes::new(view_rc.clone()));

				for id in star_systems {
					let ss = StarSystem::new(id, view_rc.clone(), handle.clone(), &scene.scene());
					let ssname = StarSystemName::new(id, view_rc.clone());
					scene.add_drawable(ss);
					scene.add_drawable(ssname);
				}

				for id in crowns {
					let crown = Crown::new(id, view_rc.clone(), &scene.scene());
					scene.add_drawable(crown);
				}

				window.set_child(Some(&vbox));
				view_state.scene.add_drawable(FullDark::new());

				let actions_paned = gtk::Paned::new(gtk::Orientation::Horizontal);
				actions_paned.set_start_child(Some(&paned));
				actions_paned.set_resize_start_child(true);
				actions_paned.set_end_child(Some(&actions_list));
				actions_paned.set_shrink_end_child(true);

				vbox.append(&actions_paned);

				*state = GuiMachineState::View(view_state)
			}
			GuiInEvent::AwaitView => {
				window.set_child(Some(&gtk::Label::new(Some("Please wait"))));
				window.present();
				if let GuiMachineState::View(view_state) = state {
					*state = GuiMachineState::AwaitingView {
						previous_zoom_level: view_state.scene.zoom_level(),
						scrolled: view_state.scrolled.clone(),
					}
				};
			}
			GuiInEvent::ActionsSent => {
				if let GuiMachineState::View(view_state) = state {
					view_state.end_turn_checkbox.set_inconsistent(false);
					view_state.end_turn_checkbox.set_active(true);
					view_state.end_turn_checkbox.set_sensitive(false);
				}
			}
			GuiInEvent::UpdateState => {
				// TODO: disable controls
				handle.broadcast_event(GuiOutEvent::GetServerState);
			}
			GuiInEvent::AddAction(action) => {
				if let GuiMachineState::View(view_state) = state {
					view_state.add_action(action);
				}
			}
			GuiInEvent::RemoveAction(action) => {
				if let GuiMachineState::View(view_state) = state {
					view_state.remove_action(&action);
				}
			}
			GuiInEvent::SetActions => {
				if let GuiMachineState::View(view_state) = state {
					handle.broadcast_event(GuiOutEvent::SubmitActions(view_state.action_cache.actions().into()));
				}
			}
			GuiInEvent::GotAffero(s) => {
				tracing::info!("Received affero: {}", s);
			}
			GuiInEvent::SetServerState(server::ServerStateView::Lobby(lobby_view)) => {
				let paned = gtk::Paned::new(gtk::Orientation::Horizontal);

				let slots_box = gtk::Box::new(gtk::Orientation::Vertical, 2);

				let player_id_strings: Vec<String> = ["".into()].into_iter().chain(lobby_view.players_connected.iter().map(hex::encode)).collect();
				let player_id_strs: Vec<&str> = ["AI"].into_iter().chain(player_id_strings.iter().map(|s| s.as_str())).collect();

				let mut assign_buttons = Vec::with_capacity(lobby_view.slots.len());
				for (i, player) in lobby_view.slots.iter().enumerate() {
					let player_box = gtk::Box::new(gtk::Orientation::Horizontal, 2);
					player_box.append(&gtk::Label::new(Some(&player.as_ref().map(|p| p.human_str()).unwrap_or_else(|| "(Empty)".into()))));
					let remove_button = gtk::Button::new();
					remove_button.set_label("Remove_slot");
					let b_handle = handle.clone();
					remove_button.connect_clicked(move |_| b_handle.broadcast_event(GuiOutEvent::RemoveSlot(i as u64)));
					player_box.append(&remove_button);

					let assign_dropdown = gtk::DropDown::from_strings(player_id_strs.as_slice());
					let assign_button = gtk::Button::new();
					assign_button.set_label("Assign");
					player_box.append(&assign_dropdown);
					player_box.append(&assign_button);

					let handle = handle.clone();
					assign_button.connect_clicked(move |_| {
						if let Some(selected_player) = assign_dropdown.selected_item() {
							let so: gtk::StringObject = selected_player.downcast().unwrap();
							let pid_s: String = so.string().to_string();
							if pid_s.is_empty() {
								handle.broadcast_event(GuiOutEvent::AssignPlayerToSlot(None, i as u64));
							} else if pid_s == "AI" {
								handle.broadcast_event(GuiOutEvent::AssignAiToSlot(i as u64));
							} else {
								let player_id: server::PlayerID = hex::decode(&pid_s).unwrap().try_into().unwrap();
								handle.broadcast_event(GuiOutEvent::AssignPlayerToSlot(Some(player_id), i as u64));
							}
						}
					});

					assign_buttons.push(assign_button.clone());

					slots_box.append(&player_box);
				}

				let lobby_box = gtk::Box::new(gtk::Orientation::Vertical, 2);
				lobby_box.append(&slots_box);

				let add_slot_button = gtk::Button::new();
				add_slot_button.set_label("Add slot");
				let b_handle = handle.clone();
				add_slot_button.connect_clicked(move |_| b_handle.broadcast_event(GuiOutEvent::AddSlot)); // TODO: disable controls
				lobby_box.append(&add_slot_button);

				let start_game_button = gtk::Button::new();
				start_game_button.set_label("Start game");
				let b_handle = handle.clone();
				start_game_button.connect_clicked(move |_| b_handle.broadcast_event(GuiOutEvent::StartGame)); // TODO: disable controls
				lobby_box.append(&start_game_button);

				let player_list_box = gtk::ListBox::new();

				for player in &lobby_view.players_connected {
					let idhex = hex::encode(player);
					let short = &idhex[0..7];
					let label = gtk::Label::new(Some(short));
					label.set_tooltip_text(Some(&idhex));
					let row = gtk::ListBoxRow::new();
					row.set_child(Some(&label));

					player_list_box.append(&row);
				}

				paned.set_start_child(Some(&lobby_box));
				paned.set_end_child(Some(&player_list_box));

				window.set_child(Some(&paned));
				window.present();
				*state = GuiMachineState::Lobby;
			}
			GuiInEvent::SetServerState(server::ServerStateView::Game) => {
				handle.send_event(GuiInEvent::AwaitView);
				handle.broadcast_event(GuiOutEvent::GetView);
			}
			GuiInEvent::StarSystemSelected { id } => {
				if let GuiMachineState::View(view_state) = state {
					let interface = view_state.view_rc.interface();
					let ss = interface.star_system(id);
					destroy_children(&view_state.info_widget);

					let mut ss_desc = String::new();

					ss_desc.push_str(&format!("<big>{}</big>: {}, radiation: {}", ss.data.name, ss.data.star_type, ss.data.star_cosmic_ray_intensity));

					if let Some(empire) = ss.empire() {
						ss_desc.push_str(&format!("\n<span foreground='{}'>{}</span>", empire.data.color.to_html(), empire.data.name))
					} else {
						ss_desc.push_str("\nneutral");
					}

					ss_desc.push_str("\nPlanets:");
					for planet in ss.planets() {
						ss_desc.push_str(&format!(
							"\nInsurance cost: {} (core: {} bio: {} atm: {})",
							Unit::<EnergyCredit>::from(planet.insurance_cost()),
							planet.data.core_spin_speed,
							planet.data.surface_bio_quality,
							planet.data.atmosphere_bio_quality
						));
						ss_desc.push_str(&format!("\nFuel gathering: {}", planet.data.fuel_availability));
						ss_desc.push_str(&format!("\nAgricultural maintenance cost: {}", Unit::<EnergyCredit>::from(planet.agriculture_cost())));
						ss_desc.push_str(&format!("\nPopulation: {}", planet.data.population,));
						ss_desc.push_str(&format!("\n{} private", planet.data.goods_storage.energy_credits,));
						ss_desc.push_str(&format!("\n{} private", planet.data.goods_storage.fuel,));
						ss_desc.push_str(&format!("\n{} private", planet.data.goods_storage.food,));
						if let Some(income_tax) = planet.income_tax() {
							ss_desc.push_str(&format!("\nTax rate: {}", income_tax.0.floatprint()));
						}

						ss_desc.push_str(&format!("\nFarmers: {}", planet.data.planetary_economics.farmers,));
						ss_desc.push_str(&format!("\nEC gatherers: {}", planet.data.planetary_economics.ec_gatherers,));
						ss_desc.push_str(&format!("\nNIL: {}", planet.data.planetary_economics.nils,));

						ss_desc.push_str("\nAssets:");
						if !planet.data.economic_asset_list.farms.is_zero() {
							ss_desc.push_str(&format!("\n{}", planet.data.economic_asset_list.farms));
						};
						if !planet.data.economic_asset_list.fuel_sifters.is_zero() {
							ss_desc.push_str(&format!("\n{}", planet.data.economic_asset_list.fuel_sifters));
						};
						ss_desc.push('\n');
					}

					if let Some(owned_empire) = interface.controlled_empire() {
						if !ss.is_own() {
							let capture_button = gtk::ToggleButton::with_label("Capture");

							let handle_clone = handle.clone();
							capture_button.set_sensitive(false);
							let action = rustorion::action::Action::CaptureStarSystem {
								star_system_id: ss.data.id.cast(),
								empire_id: owned_empire.data.id.cast(),
							};
							capture_button.set_active(view_state.action_cache.actions().contains(&action));
							let capture_price = view_state.view_rc.interface().star_system(ss.data.id).capture_price();
							capture_button.set_label(&format!("Capture ({capture_price})"));

							if ss.can_capture(owned_empire) && view_state.action_cache.budget >= capture_price {
								capture_button.set_sensitive(true);

								capture_button.connect_toggled(move |c| {
									if c.is_active() {
										handle_clone.send_event(GuiInEvent::AddAction(action.clone()));
									} else {
										handle_clone.send_event(GuiInEvent::RemoveAction(action.clone()));
									};
								});
							} else {
								capture_button.set_sensitive(false);
							}

							view_state.info_widget.prepend(&capture_button);
						} else {
							let fleets_left = view_state.action_cache.budget.div(view_state.view_rc.ship_cost).div(2).floor().to_u64().unwrap();
							let fleet_prod_box = gtk::Box::new(gtk::Orientation::Vertical, 0);
							let fleet_prod_spin = gtk::SpinButton::new(Some(&gtk::Adjustment::new(0., 0., (fleets_left + 1) as f64, 1., 1., 1.)), 1.0, 0);
							fleet_prod_box.append(&fleet_prod_spin);

							let fleet_prod_button = gtk::Button::with_label("Build fleets");
							if ss.can_build_fleet(owned_empire) {
								let handle_clone = handle.clone();
								let star_system_id = ss.data.id;
								fleet_prod_button.connect_clicked(move |_| {
									let number = fleet_prod_spin.value_as_int() as u64;
									if number > 0 {
										let action = rustorion::action::Action::BuildFleets {
											star_system_id,
											fleets_number: number,
										};
										handle_clone.send_event(GuiInEvent::AddAction(action));
									}
									fleet_prod_spin.set_value(0.0);
									fleet_prod_spin.set_range(0.0, (fleets_left - number) as f64);
								});
							} else {
								fleet_prod_button.set_tooltip_text(Some("Can't build fleets here"));
								fleet_prod_button.set_sensitive(false);
								fleet_prod_spin.set_sensitive(false);
							};
							fleet_prod_box.append(&fleet_prod_button);

							view_state.info_widget.append(&fleet_prod_box);
						}
					};

					let ss_label = gtk::Label::new(None);
					ss_label.set_markup(&ss_desc);

					let buffer = gtk::TextBuffer::default();

					let mut text = String::new();

					for trade_event in ss.related_trade_events() {
						text.push_str(&format!("{trade_event}\n"));
					}

					buffer.set_text(&text);

					let trade_log = gtk::TextView::with_buffer(&buffer);
					trade_log.set_editable(false);
					trade_log.set_wrap_mode(gtk::WrapMode::Word);
					trade_log.set_hexpand(true);
					let trade_log_frame = gtk::Frame::new(Some("Trade events"));
					let trade_scrollable = gtk::ScrolledWindow::new();
					trade_scrollable.set_child(Some(&trade_log));
					trade_log_frame.set_child(Some(&trade_scrollable));

					view_state.info_widget.append(&trade_log_frame);

					let buffer = gtk::TextBuffer::default();

					let mut text = String::new();

					for migration_event in ss.related_migration_events() {
						text.push_str(&format!("{migration_event}\n"));
					}

					buffer.set_text(&text);

					let migration_log = gtk::TextView::with_buffer(&buffer);
					migration_log.set_editable(false);
					migration_log.set_wrap_mode(gtk::WrapMode::Word);
					migration_log.set_hexpand(true);
					let migration_log_frame = gtk::Frame::new(Some("Migration events"));
					let migration_scrollable = gtk::ScrolledWindow::new();
					migration_scrollable.set_child(Some(&migration_log));
					migration_log_frame.set_child(Some(&migration_scrollable));

					view_state.info_widget.append(&migration_log_frame);

					view_state.info_widget.prepend(&ss_label);

					if let Some((drawing_index, _)) = view_state.focus {
						view_state.scene.remove_drawable(drawing_index);
					}
					let ss = StarSystem::new(id, view_state.view_rc.clone(), handle.clone(), &view_state.scene.scene());
					let reticle = Reticle::star_system(ss, &view_state.scene.scene());

					let drawing_index = view_state.scene.add_drawable(reticle.clone());

					view_state.focus = Some((drawing_index, reticle));

					view_state.scene.queue_draw();
				}
			}
			GuiInEvent::FleetSelected { fleet_id, stack_number } => {
				if let GuiMachineState::View(view_state) = state {
					let interface = view_state.view_rc.interface();
					let fleet = interface.fleet(fleet_id);
					let star_system = fleet.star_system();
					let empire = fleet.empire();
					let empire_name = &empire.data.name;
					let ships = fleet.ships().len();
					destroy_children(&view_state.info_widget);
					view_state.info_widget.prepend(&gtk::Label::new(Some(&format!(
						"{} fleet \"{}\" of {} ships, orbiting {}",
						empire_name, fleet.data.name, ships, star_system.data.name
					))));

					let ships_list = gtk::ListBox::default();
					for ship in fleet.ships() {
						ships_list.append(&gtk::Label::new(Some(&ship.data.name)));
					}
					view_state.info_widget.append(&ships_list);

					let ships_left = view_state.action_cache.budget.div(view_state.view_rc.ship_cost).floor().to_i64().unwrap();
					let ship_prod_box = gtk::Box::new(gtk::Orientation::Vertical, 0);
					let ship_prod_spin = gtk::SpinButton::new(Some(&gtk::Adjustment::new(0., 0., (ships_left + 1) as f64, 1., 1., 1.)), 1.0, 0);
					ship_prod_box.append(&ship_prod_spin);

					let ship_prod_button = gtk::Button::with_label("Build ships");
					let fleet = interface.fleet(fleet.data.id.cast());
					if fleet.can_build_ships() {
						let handle_clone = handle.clone();
						let fleet_id = fleet.data.id;
						ship_prod_button.connect_clicked(move |_| {
							let number = ship_prod_spin.value_as_int() as i64;
							if number > 0 {
								let action = rustorion::action::Action::BuildShips { fleet_id, ships_number: number };
								handle_clone.send_event(GuiInEvent::AddAction(action));
							}
							ship_prod_spin.set_value(0.0);
							ship_prod_spin.set_range(0.0, (ships_left - number) as f64);
						});
					} else {
						ship_prod_button.set_tooltip_text(Some("Can't build ships here"));
						ship_prod_button.set_sensitive(false);
						ship_prod_spin.set_sensitive(false);
					};
					ship_prod_box.append(&ship_prod_button);

					view_state.info_widget.append(&ship_prod_box);

					let buffer = gtk::TextBuffer::default();
					let mut text = String::new();
					for trade_event in fleet.related_trade_events() {
						text.push_str(&format!("{trade_event}\n"));
					}

					buffer.set_text(&text);
					let trade_log = gtk::TextView::with_buffer(&buffer);
					trade_log.set_editable(false);
					trade_log.set_wrap_mode(gtk::WrapMode::Word);
					let trade_log_frame = gtk::Frame::new(Some("Trade events"));
					let trade_scrollable = gtk::ScrolledWindow::new();
					trade_scrollable.set_child(Some(&trade_log));
					trade_log.set_hexpand(true);
					trade_log_frame.set_child(Some(&trade_scrollable));

					view_state.info_widget.append(&trade_log_frame);

					if let Some((drawing_index, _)) = view_state.focus {
						view_state.scene.remove_drawable(drawing_index);
					}
					let fleet = Fleet::new(fleet_id, view_state.view_rc.clone(), stack_number, handle.clone(), &view_state.scene.scene());
					let reticle = Reticle::fleet(fleet, &view_state.scene.scene());
					let drawing_index = view_state.scene.add_drawable(reticle.clone());

					view_state.focus = Some((drawing_index, reticle));

					view_state.scene.queue_draw();
				}
			}
			GuiInEvent::StarSystemTargeted { star_system_id: target_system_id } => {
				if let GuiMachineState::View(view_state) = state {
					match &view_state.focus {
						Some((
							_,
							Reticle {
								reticle_kind: ReticleKind::Fleet(fleet),
								..
							},
						)) => {
							let ui = view_state.view_rc.interface();
							let fleet = ui.fleet(fleet.fleet_id);
							let star_system = fleet.star_system();

							// see if it is a valid order
							// you own the fleet
							let owned = ui.controlled_empire() == Some(fleet.empire());
							// there is a starlane
							let other_star_system = ui.star_system(target_system_id);
							let connected = target_system_id == fleet.star_system().data.id || star_system.starlane_to(other_star_system);
							if owned && connected {
								let actions_to_delete: Vec<_> = view_state
									.action_cache
									.actions()
									.iter()
									.filter(|a| {
										if let rustorion::action::Action::MoveFleet { fleet_id, .. } = a {
											*fleet_id == fleet.data.id
										} else {
											false
										}
									})
									.cloned()
									.collect();
								let fleet_star_system_id = fleet.star_system().data.id;

								let fleet_id = fleet.data.id.cast();

								for action_to_delete in &actions_to_delete {
									view_state.remove_action(action_to_delete);
								}

								if target_system_id != fleet_star_system_id {
									view_state.add_action(rustorion::action::Action::MoveFleet {
										fleet_id,
										target_star_system_id: target_system_id.cast(),
									});
								};
							}
						}
						Some((
							_,
							Reticle {
								reticle_kind: ReticleKind::StarSystem(star_system),
								..
							},
						)) => {
							// Plot course
							let target_star_system = view_state.view_rc.interface().star_system(target_system_id);
							let current_star_system = view_state.view_rc.interface().star_system(star_system.id);
							if let Some((path, cost)) = current_star_system.shipping_path_cost(target_star_system, view_state.view_rc.interface().controlled_empire()) {
								tracing::warn!("Path cost {}", cost);
								for ss in path {
									tracing::warn!("thru {}", ss.data.name);
								}
							} else {
								tracing::warn!("No path")
							}
						}
						_ => (),
					}
				}
			}
			GuiInEvent::Quit => {
				application.quit();
			}
		};
	}
	fn run(self) {
		let application = gtk::Application::new(Some("net.bitcheese.rustorion.gui-client"), gio::ApplicationFlags::default() | gio::ApplicationFlags::NON_UNIQUE);

		let mut receiver = self.driver.guts.receiver;
		let mut state = GuiMachineState::AwaitingView {
			previous_zoom_level: 0,
			scrolled: None,
		};

		let (main_sender, mut main_receiver) = futures::channel::mpsc::channel(1);
		let event_application = application.clone();

		glib::MainContext::default().spawn_local(async move {
			let () = main_receiver.next().await.unwrap();
			loop {
				let event = receiver.next().await;
				if let Some(event) = event {
					Self::process_event(&self.driver.handle, &event_application, &mut state, event);
				} else {
					break;
				}
			}
		});

		application.connect_activate(move |app| {
			let window = gtk::ApplicationWindow::new(app);
			window.set_title(Some("Rustorion"));
			window.present();

			let mut sender = main_sender.clone();
			sender.try_send(()).unwrap();

			// TODO: something is broken
			// 		gio::prelude::SettingsExt::set_boolean(&gtk::Settings::default().unwrap(), "gtk-application-prefer-dark-theme", true);
		});

		let args: [&str; 0] = [];
		gtk::init().unwrap();
		application.run_with_args(&args);
	}
}

#[allow(clippy::large_enum_variant)]
#[derive(Debug, Clone)]
enum GuiInEvent {
	SetView { view: universeview::Universe, local: bool },
	SetServerState(server::ServerStateView),
	AwaitView,
	ActionsSent, // TODO: send ID to make sure THESE actions have been sent
	AddAction(rustorion::action::Action),
	RemoveAction(rustorion::action::Action),
	SetActions,
	GotAffero(String),
	UpdateState,
	StarSystemSelected { id: ID<universeview::StarSystem> },
	FleetSelected { fleet_id: ID<universeview::Fleet>, stack_number: u64 },
	StarSystemTargeted { star_system_id: ID<universeview::StarSystem> },
	Quit,
}

#[derive(Debug, Clone)]
enum GuiOutEvent {
	SubmitActions(Vec<rustorion::action::Action>),
	GetServerState,
	GetView,
	AddSlot,
	RemoveSlot(u64),
	AssignPlayerToSlot(Option<server::PlayerID>, u64),
	AssignAiToSlot(u64),
	StartGame,
}

#[derive(Default)]
struct GuiMachine {
	driver: FutureDriver<Self>,
}

impl state_machine::AsyncEventMachine for GuiMachine {
	type InEvent = GuiInEvent;
	type OutEvent = GuiOutEvent;
	type Driver = FutureDriver<Self>;
}

impl CanSubscribe<client::Client> for GuiMachine {
	fn translate_event(event: client::ClientEvent) -> Vec<Self::InEvent> {
		match event {
			client::ClientEvent::StateChanged(_) => vec![GuiInEvent::UpdateState],
			client::ClientEvent::GotView(view) => vec![GuiInEvent::SetView { view: *view, local: false }],
			client::ClientEvent::GotEmpireID(_) => vec![],
			client::ClientEvent::ActionsSet => vec![GuiInEvent::ActionsSent],
			client::ClientEvent::GotAffero(s) => vec![GuiInEvent::GotAffero(s)],
			client::ClientEvent::GotServerState(s) => vec![GuiInEvent::SetServerState(s)],
		}
	}
}

impl CanSubscribe<GuiMachine> for client::Client {
	fn translate_event(event: GuiOutEvent) -> Vec<Self::InEvent> {
		match event {
			GuiOutEvent::SubmitActions(actions) => vec![client::ClientInEvent::SetActions(actions)],
			GuiOutEvent::GetView => vec![client::ClientInEvent::GetView],
			GuiOutEvent::GetServerState => vec![client::ClientInEvent::GetServerState],
			GuiOutEvent::AddSlot => vec![client::ClientInEvent::AddSlot],
			GuiOutEvent::RemoveSlot(slot_n) => vec![client::ClientInEvent::RemoveSlot(slot_n)],
			GuiOutEvent::AssignPlayerToSlot(player_id_opt, slot_n) => vec![client::ClientInEvent::AssignPlayerToSlot(player_id_opt, slot_n)],
			GuiOutEvent::AssignAiToSlot(slot_n) => vec![client::ClientInEvent::AssignAiToSlot(slot_n)],
			GuiOutEvent::StartGame => vec![client::ClientInEvent::StartGame],
		}
	}
}

impl CanSubscribe<server::Server> for GuiMachine {
	fn translate_event(event: server::ServerOutEvent) -> Vec<Self::InEvent> {
		match event {
			server::ServerOutEvent::StateChanged(_state_n) => vec![],
		}
	}
}

struct ViewState {
	scene: librae_gui::scene::SceneWidget,
	scrolled: Option<gtk::ScrolledWindow>,
	actions_list: gtk::ListBox,
	info_widget: gtk::Box,
	view_rc: Rc<universeview::Universe>,
	action_cache: rustorion::action::ActionCache,
	action_drawables: Vec<DrawableIndex>,
	end_turn_checkbox: gtk::CheckButton,
	focus: Option<(DrawableIndex, Reticle)>,
}

#[allow(clippy::large_enum_variant)]
enum GuiMachineState {
	View(ViewState),
	AwaitingView { previous_zoom_level: usize, scrolled: Option<gtk::ScrolledWindow> },
	Lobby,
	// 	Exit,
}

#[tokio::main]
async fn main() -> Result<()> {
	rustorion::ensure_tracing_subscriber();

	let gui_machine = GuiMachine::default();
	let gui_handle = gui_machine.driver.handle.clone();

	let options = Options::parse();

	let mut futures = futures::stream::FuturesUnordered::new();

	fn connect_to(hostport: String, auth_data: rustorion::net::AuthData, gui_handle: &FutureHandle<GuiMachine>) -> Result<tokio::task::JoinHandle<Result<()>>> {
		let client = rustorion::net::client::Client::default();

		client.0.handle.add_subscriber(gui_handle);
		gui_handle.add_subscriber(&client.0.handle);
		client.0.handle.send_event(client::ClientInEvent::GetAffero);
		client.0.handle.send_event(client::ClientInEvent::GetServerState);
		Ok(tokio::task::spawn(client.connect(hostport, auth_data)))
	}

	match options.action {
		Action::CreateServer(server_options) => {
			let auth_data = rustorion::net::AuthData::obtain_by_name(&server_options.authdata_name)?;
			let mut server = rustorion::net::server::Server::default();
			server.setup_state(&server_options)?;
			server.driver.handle.add_subscriber(&gui_handle);
			let addr = server_options.addr.clone();
			let server_task = server.run(server_options).await?;

			futures.push(server_task);
			futures.push(connect_to(addr, auth_data, &gui_handle)?);
		}
		Action::Connect { hostport, authdata_name } => {
			let auth_data = rustorion::net::AuthData::obtain_by_name(&authdata_name)?;
			futures.push(connect_to(hostport, auth_data, &gui_handle)?);
		}
		Action::LoadView => {
			let mut view_data = vec![];
			tokio::io::stdin().read_to_end(&mut view_data).await?;
			let view = ciborium::from_reader(&*view_data)?;
			gui_handle.send_event(GuiInEvent::SetView { view, local: true });
		}
	};

	futures.push(tokio::task::spawn_blocking(move || {
		gui_machine.run();
		Ok(())
	}));

	let finished_future = futures.next().await;
	if let Some(Ok(Ok(()))) = finished_future {
		tracing::debug!("A future finished");
	} else {
		tracing::warn!("{:?}", finished_future)
	};
	gui_handle.send_event(GuiInEvent::Quit);
	Ok(())
}
